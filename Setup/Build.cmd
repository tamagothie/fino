@echo off

set config=Release
set outputdir=..\..\..\Setup\Release
set commonflags=/p:Configuration=%config%;AllowUnsafeBlocks=true /p:CLSCompliant=False

if %PROCESSOR_ARCHITECTURE%==x86 (
         set msbuild="%WINDIR%\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
) else ( set msbuild="%WINDIR%\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe"
)

:build
echo ---------------------------------------------------------------------
echo Building AnyCpu release...
%msbuild% ..\Apps\FinoBuild.sln %commonflags% /tv:4.0 /p:TargetFrameworkVersion=v4.0 /p:Platform="Any Cpu" /p:OutputPath="%outputdir%"
if errorlevel 0 goto done

:build-error
echo Failed to compile...
pause
goto exit

:done
echo.
echo ---------------------------------------------------------------------
echo Compile finished....

echo Creating installer....
makensis installer.nsi
if errorlevel 0 goto finish

:installer-error
echo.
echo ---------------------------------------------------------------------
echo Compile is finished, but failed to create installer...
pause
goto exit

:finish
echo.
echo ---------------------------------------------------------------------
echo Compile finished (all build release files are saved in (FinoProjectDir)\Setup\Release)....
echo Create installer finished....
pause

:exit