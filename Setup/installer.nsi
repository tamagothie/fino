; installer.nsi
;
; Script installer for fino application

;--------------------------------

; The name of the installer
Name "installer"

; The file to write
OutFile "Installer\FinoInstaller.exe"

; The default installation directory
InstallDir $PROGRAMFILES\Fino

RequestExecutionLevel admin

;--------------------------------

; Pages

Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "" ;No components page, name is not important

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Put file there
  File /r Release\*.*  
  
  WriteUninstaller "uninstall.exe"
  
SectionEnd ; end the section

Section "SQLServerClearTypes" SEC01  
  File "SQLSysClrTypes.msi"
  ExecWait 'msiexec /i "$INSTDIR\SQLSysClrTypes.msi" /passive '
SectionEnd

Section "ReportViewer" SEC02
  File "ReportViewer.msi"
  ExecWait 'msiexec /i "$INSTDIR\ReportViewer.msi" /passive '
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Fino"
  CreateShortCut "$SMPROGRAMS\Fino\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\Fino\Fino.lnk" "$INSTDIR\Fino.Cient.exe" "" "$INSTDIR\Fino.Cient.exe" 0
  
SectionEnd

; Uninstaller

Section "Uninstall"

  ; Remove files and uninstaller
  Delete $INSTDIR\*.*  

  ; Remove directories used
  RMDir "$INSTDIR"

  Delete "$SMPROGRAMS\Fino\Uninstall.lnk"
  Delete "$SMPROGRAMS\Fino\Fino.lnk"
  RMDir  "$SMPROGRAMS\Fino"  

SectionEnd
