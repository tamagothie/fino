﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public class Constants
    {
        public List<ValueList> Bulan { get; set; }
        public List<string> MenuGroupHeader { get; set; }
        public List<ValueList> Repetisi { get; set; }

        public const string FontName = "Verdana";
        public const float FontSize = 10;

        public enum MenuEnums
        {
            PendaftaranSiswaBaru,
            DaftarUlang,
            PotonganBiaya,
            Jemputan,
            Katering,
            JenisBiaya,
            GrupBiaya,
            Setoran,
            Penarikan,
            LaporanTabungan,
            KodeAkun,
            PostingJurnal,
            PostingBukuBesar,
            LaporanAkuntansi,
            LaporanPiutang,
            ReportRPP
        }

        #region Singleton
        private static readonly Lazy<Constants> lazy = new Lazy<Constants>(() => new Constants());
    
        public static Constants Instance { get { return lazy.Value; } }

        private Constants() 
        {
            GenerateBulan();
            GenerateMenuGroupHeader();
            GenerateRepetisi();
        }

        private void GenerateRepetisi()
        {
            Repetisi = new List<ValueList>();

            Repetisi.Add(new ValueList() { Id = 0, Value = "NONE" });
            Repetisi.Add(new ValueList() { Id = 1, Value = "MINGGUAN" });
            Repetisi.Add(new ValueList() { Id = 2, Value = "BULANAN" });
            Repetisi.Add(new ValueList() { Id = 3, Value = "TAHUNAN" });
            Repetisi.Add(new ValueList() { Id = 4, Value = "TAHUNAJARAN" });
            Repetisi.Add(new ValueList() { Id = 5, Value = "SEMESTER" });
            Repetisi.Add(new ValueList() { Id = 6, Value = "CAWU" });
        }

        private void GenerateBulan()
        {
            Bulan = new List<ValueList>();

            Bulan.Add(new ValueList() { Id = 0, Value = "None" });
            Bulan.Add(new ValueList() { Id = 1, Value = "Januari" });
            Bulan.Add(new ValueList() { Id = 2, Value = "Februari" });
            Bulan.Add(new ValueList() { Id = 3, Value = "Maret" });
            Bulan.Add(new ValueList() { Id = 4, Value = "April" });
            Bulan.Add(new ValueList() { Id = 5, Value = "Mei" });
            Bulan.Add(new ValueList() { Id = 6, Value = "Juni" });
            Bulan.Add(new ValueList() { Id = 7, Value = "Juli" });
            Bulan.Add(new ValueList() { Id = 8, Value = "Agustus" });
            Bulan.Add(new ValueList() { Id = 9, Value = "September" });
            Bulan.Add(new ValueList() { Id = 10, Value = "Oktober" });
            Bulan.Add(new ValueList() { Id = 11, Value = "November" });
            Bulan.Add(new ValueList() { Id = 12, Value = "Desember" });            
        }

        private void GenerateMenuGroupHeader()
        {
            MenuGroupHeader = new List<string>();

            MenuGroupHeader.Add("Siswa");
            MenuGroupHeader.Add("Biaya");
            MenuGroupHeader.Add("Tabungan");
            MenuGroupHeader.Add("Akuntansi");
            MenuGroupHeader.Add("Kas");
        }
        #endregion
    }

    public class ValueList
    {
        public object Id { get; set; }
        public object Value { get; set; }
    }
}
