﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public interface IView
    {
        event EventHandler Load;

        void Show();
        void Hide();
        void Close();
        bool Focus();
    }
}
