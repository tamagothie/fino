﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public enum KelasTingkatEnum
    {
        TK_1 = 1,
        TK_2,
        TK_3,
        TK_4,
        TK_5,
        TK_6,
        TK_7,
        TK_8,
        TK_9,
        TK_10
    }
}
