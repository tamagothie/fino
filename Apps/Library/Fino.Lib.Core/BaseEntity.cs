﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Lib.Core
{
    public class BaseModel : INotifyPropertyChanged
    {
        public string ValidationMessage { get; set; }
        public event EventHandler<CustomEventArgs<string>> ErrorValidation;

        #region Implementation of INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, e);
        }

        public void ValidateProperty(object p_Value, string p_NamaProperty)
        {
            try
            {
                Validator.ValidateProperty(p_Value,
                    new ValidationContext(this, null, null) { MemberName = p_NamaProperty });
                ValidationMessage = "";
            }
            catch (ValidationException ve)
            {
                ValidationMessage = ve.Message;

                if (ErrorValidation != null)
                {
                    ErrorValidation(this, new CustomEventArgs<string>(ValidationMessage));
                }
            }
        }

        #endregion
    }
}
