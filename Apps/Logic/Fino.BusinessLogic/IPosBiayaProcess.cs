﻿using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IPosBiayaProcess
    {
        ProcessResult GetRekapitulasiPiutangDanPenerimaan(DateTime p_FromDate, DateTime p_ToDate);
    }
}
