﻿using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Fino.BusinessLogic
{
    public class UserLogonProcess : IUserLogonProcess
    {
        public ProcessResult UserLogon(string p_UserName, string p_Password)
        {
            ProcessResult result = new ProcessResult();

            using (IUserRepository repo = new UserRepository())
            {
                SysUser user = repo.UserLogon(p_UserName, p_Password);

                if (user != null)
                {
                    result.IsSucess = true;
                }
                else
                {
                    result.IsSucess = false;
                    result.ProcessException = new Exception("Username atau password salah");
                }
            }
            

            return result;
        }

        public ProcessResult UserLogoff(string p_UserName)
        {
            throw new NotImplementedException();
        }
    }
}
