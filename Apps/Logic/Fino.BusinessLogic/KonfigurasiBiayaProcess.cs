﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using Fino.Datalib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class KonfigurasiBiayaProcess : IKonfigurasiBiayaProcess
    {
        public ProcessResult GetRefBiaya()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    List<RefBiaya> resultData = repo.KonfigurasiBiayaData();
                    List<RefBiayaModel> resultModel = new List<RefBiayaModel>(resultData.Count);

                    foreach (RefBiaya item in resultData)
                    {
                        resultModel.Add(AutoMapper.Mapper.Map<RefBiayaModel>(item));
                    }

                    result.DataResult = resultModel;
                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetRefBiaya(int p_Id)
        {
            throw new NotImplementedException();
        }

        public ProcessResult TambahRefBiaya(RefBiayaModel p_KonfigurasiBiayaModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    RefBiaya refBiaya = AutoMapper.Mapper.Map<RefBiaya>(p_KonfigurasiBiayaModel);
                    repo.AddKonfigurasiBiayaData(refBiaya);
                    repo.SaveChanges();

                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdateRefBiaya(RefBiayaModel p_KonfigurasiBiayaModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    RefBiaya refBiaya = AutoMapper.Mapper.Map<RefBiaya>(p_KonfigurasiBiayaModel);
                    repo.UpdateKonfigurasiBiayaData(refBiaya);
                    repo.SaveChanges();

                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeleteRefBiaya(RefBiayaModel p_KonfigurasiBiayaModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    RefBiaya refBiaya = AutoMapper.Mapper.Map<RefBiaya>(p_KonfigurasiBiayaModel);
                    repo.DeleteKonfigurasiBiayaData(refBiaya);
                    repo.SaveChanges();

                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }


        public ProcessResult GetGrupBiaya()
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    List<RefGrupBiaya> resultData = repo.GrupBiayaData();
                    List<GrupBiayaModel> resultModel = new List<GrupBiayaModel>(resultData.Count);

                    foreach (RefGrupBiaya item in resultData)
                    {
                        resultModel.Add(AutoMapper.Mapper.Map<GrupBiayaModel>(item));
                    }

                    result.DataResult = resultModel;
                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetGrupBiaya(int p_Id)
        {
            throw new NotImplementedException();
        }

        public ProcessResult TambahGrupBiaya(GrupBiayaModel p_grupBiayaModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    RefGrupBiaya refGrupBiaya = AutoMapper.Mapper.Map<RefGrupBiaya>(p_grupBiayaModel);
                    repo.AddGrupBiayaData(refGrupBiaya);
                    repo.SaveChanges();

                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult UpdateGrupBiaya(GrupBiayaModel p_grupBiayaModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    RefGrupBiaya refGrupBiaya = AutoMapper.Mapper.Map<RefGrupBiaya>(p_grupBiayaModel);
                    repo.UpdateGrupBiayaData(refGrupBiaya);
                    repo.SaveChanges();

                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult DeleteGrupBiaya(GrupBiayaModel p_grupBiayaModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    RefGrupBiaya refGrupBiaya = AutoMapper.Mapper.Map<RefGrupBiaya>(p_grupBiayaModel);
                    repo.DeleteGrupBiayaData(refGrupBiaya);
                    repo.SaveChanges();

                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }


        public ProcessResult UpdateMappingGrupBiaya(GrupBiayaModel p_grupBiayaModel)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    RefGrupBiaya refGrupBiaya = AutoMapper.Mapper.Map<RefGrupBiaya>(p_grupBiayaModel);

                    List<GrupBiaya> grupBiayas = new List<GrupBiaya>();

                    foreach (var item in p_grupBiayaModel.RefBiayas)
                    {
                        GrupBiaya gb = new GrupBiaya();
                        gb.GrupBiaya_Id = p_grupBiayaModel.GrupBiayaId;
                        gb.Biaya_Id = item.Biaya_id;

                        grupBiayas.Add(gb);
                    }

                    repo.UpdateMappingGrupBiayaData(grupBiayas, refGrupBiaya);
                    repo.SaveChanges();

                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }


        public ProcessResult GetMappingBiaya(int p_Id)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IKonfigurasiBiayaRepository repo = new KonfigurasiBiayaRepository())
                {
                    List<GrupBiaya> repoResult = repo.GetMappingGrupBiayaData(p_Id);
                    List<RefBiayaModel> refBiaya = null;

                    if (repoResult != null && repoResult.Count > 0)
                    {
                        refBiaya = new List<RefBiayaModel>(repoResult.Count);

                        foreach (GrupBiaya item in repoResult)
                        {
                            RefBiayaModel pbModel = AutoMapper.Mapper.Map<RefBiayaModel>(item.Biaya);

                            refBiaya.Add(pbModel);
                        }
                    }

                    result.IsSucess = true;
                    result.DataResult = refBiaya;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
