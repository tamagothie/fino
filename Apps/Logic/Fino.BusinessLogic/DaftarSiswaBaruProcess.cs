﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;

namespace Fino.BusinessLogic
{
    public class DaftarSiswaBaruProcess : IDaftarSiswaBaruProcess
    {
        void ValidateData(DaftarSiswaBaruModel model)
        {
            if (string.IsNullOrEmpty(model.Nama)
            || string.IsNullOrEmpty(model.Code))
            {
                throw new ApplicationException(AppResource.MSG_INCOMPLETE_DATA);
            }
        }

        public Lib.Core.ProcessResult TambahSiswaBaru(DaftarSiswaBaruModel model)
        {
            ProcessResult result = new ProcessResult();
            var data = model;
            try
            {
                ValidateData(data);

                IDaftarSiswaBaruRepository repo = new DaftarSiswaBaruRepository();
                data = repo.TambahSiswaBaru(model);

                result.IsSucess = true;
                result.DataResult = data;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetAllKelas()
        {
            ProcessResult result = new ProcessResult();
            try
            {
                IRefKelasRepository repo = new RefKelasRepository();
                result.DataResult = repo.SelectAllKelas().Select(e => new ValueList{ Id = e.kelas_id, Value = e.nama }).ToList();

                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }

        public ProcessResult GetTahunAjaran(DateTime currentDate)
        {
            ProcessResult result = new ProcessResult();
            try
            {
                ITahunAjaranRepository repo = new TahunAjaranRepository();
                var refTA = repo.GetValidTahunAjaran(currentDate.Year, currentDate.Month);
                if (refTA != null)
                {
                    result.DataResult = new ValueList
                    {
                        Id = refTA.tahunajaran_id,
                        Value = refTA.nama
                    };
                }
                result.IsSucess = true;
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
