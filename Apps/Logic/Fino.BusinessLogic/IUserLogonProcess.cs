﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public interface IUserLogonProcess
    {
        ProcessResult UserLogon(string p_UserName, string p_Password);

        ProcessResult UserLogoff(string p_UserName);
    }
}
