﻿using Fino.Lib.Core;
using Fino.Model;
using Fino.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.BusinessLogic
{
    public class PosBiayaProcess : IPosBiayaProcess
    {
        public ProcessResult GetRekapitulasiPiutangDanPenerimaan(DateTime p_FromDate, DateTime p_ToDate)
        {
            ProcessResult result = new ProcessResult();

            try
            {
                using (IPosBiayaRepository repo = new PosBiayaRepository())
                {
                    List<RekapitulasiPiutangDanPenerimaanModel> dataResult = repo.GetRekapitulasiPiutangDanPenerimaan(p_FromDate, p_ToDate);

                    result.DataResult = dataResult;
                    result.IsSucess = true;
                }
            }
            catch (Exception ex)
            {
                result.IsSucess = false;
                result.ProcessException = ex;
            }

            return result;
        }
    }
}
