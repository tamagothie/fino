﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;

namespace Fino.BusinessLogic
{
    public interface IDaftarSiswaBaruProcess
    {
        ProcessResult TambahSiswaBaru(DaftarSiswaBaruModel model);
        ProcessResult GetTahunAjaran(DateTime currentDate);
        ProcessResult GetAllKelas();
    }
}
