﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fino.Presenter
{
    public class GrupBiayaPresenter
    {
        IGrupBiayaView _grupBiayaView;
        IKonfigurasiBiayaProcess _konfigurasiBiayaProcess;

        public GrupBiayaPresenter(IGrupBiayaView p_view, IKonfigurasiBiayaProcess p_process)
        {
            _grupBiayaView = p_view;
            _konfigurasiBiayaProcess = p_process;

            _grupBiayaView.BtnTambahClick += BtnTambahClick;
            _grupBiayaView.BtnHapusClick += BtnHapusClick;
            _grupBiayaView.BtnTutupClick += BtnTutupClick;
            _grupBiayaView.GvGrupBiayaDoubleClick += GvGrupBiayaDoubleClick;
            _grupBiayaView.Load += Load;
            _grupBiayaView.ReloadData += ReloadData;
            _grupBiayaView.BtnKonfigurasiBiayaClick += BtnKonfigurasiBiayaClick;
        }

        public void BtnKonfigurasiBiayaClick()
        {
            _grupBiayaView.ShowKonfigurasiBiayaView();
        }

        public void ReloadData()
        {
            LoadData();
        }

        public void Load(object sender, EventArgs e)
        {
            LoadData();
        }

        public void GvGrupBiayaDoubleClick()
        {
            _grupBiayaView.ShowEditGrupBiayaView();
        }

        public void BtnTutupClick()
        {
            _grupBiayaView.Close();
        }

        public void BtnHapusClick()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _konfigurasiBiayaProcess.DeleteGrupBiaya(_grupBiayaView.GetSelectedGrupBiaya())),
                AppResource.MSG_DELETE_GRUP_BIAYA);

            if (result.IsSucess)
            {
                LoadData();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }

        public void BtnTambahClick()
        {
            _grupBiayaView.ShowTambahGrupBiayaView();
        }

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _konfigurasiBiayaProcess.GetGrupBiaya()),
                AppResource.MSG_LOADING_GRUP_BIAYA);

            if (result.IsSucess)
            {
                List<GrupBiayaModel> data = result.DataResult as List<GrupBiayaModel>;

                _grupBiayaView.SetData(data);
                _grupBiayaView.RefreshDataGrid();
            }
            else
            {
                WinApi.ShowErrorMessage(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE);
            }
        }
    }
}
