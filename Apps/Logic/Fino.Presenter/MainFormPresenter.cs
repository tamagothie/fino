﻿using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Presenter
{
    public class MainFormPresenter
    {
        private readonly IMainFormView _MainFormView;

        public MainFormPresenter(IMainFormView p_MainFormView)
        {
            _MainFormView = p_MainFormView;

            _MainFormView.Load += OnMainFormLoad;
            _MainFormView.KonfigurasiBiayaClicked +=_MainFormView_KonfigurasiBiayaClicked;
            _MainFormView.PendaftaranSiswaBaruClicked +=_MainFormView_PendaftaranSiswaBaruClicked;
            // _MainFormView.ShowGrupBiayaClicked += ShowGrupBiayaClicked;
            _MainFormView.ReportRPPClicked += ReportRPPClicked;
        }

        public void ReportRPPClicked()
        {
            _MainFormView.ShowReportRPPView();
        }

        public void ShowGrupBiayaClicked()
        {
            _MainFormView.ShowGrupBiayaView();
        }

        private void _MainFormView_PendaftaranSiswaBaruClicked()
        {
            _MainFormView.ShowDaftarSiswaBaruView();
        }

        void _MainFormView_KonfigurasiBiayaClicked()
        {
            _MainFormView.ShowKonfigurasiBiayaView();
        }

        public void OnMainFormLoad(object sender, EventArgs e)
        {
        }

        public void OnManageSiswaMenuClicked()
        {
            _MainFormView.ShowManageSiswaView();
        }

        public void PendaftaranSiswaBaruClicked()
        {
            _MainFormView.ShowDaftarSiswaBaruView();
        }
    }
}
