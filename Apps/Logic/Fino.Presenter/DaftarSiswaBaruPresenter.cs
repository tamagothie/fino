﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Model;
using Fino.View;

namespace Fino.Presenter
{
    public class DaftarSiswaBaruPresenter
    {
        private readonly IDaftarSiswaBaruProcess _daftarSiswaBaruProcess;
        private readonly IDaftarSiswaBaruView _daftarSiswaBaruView;

        public DaftarSiswaBaruPresenter(IDaftarSiswaBaruProcess p_Process, IDaftarSiswaBaruView p_View)
        {
            _daftarSiswaBaruProcess = p_Process;
            _daftarSiswaBaruView = p_View;
            _daftarSiswaBaruView.Load += p_View_Load;
            _daftarSiswaBaruView.PotonganButtonClicked += _daftarSiswaBaruView_PotonganButtonClicked;
            _daftarSiswaBaruView.JemputanButtonClicked += _daftarSiswaBaruView_JemputanButtonClicked;
            _daftarSiswaBaruView.SaveButtonClicked += _daftarSiswaBaruView_SaveButtonClicked;
            _daftarSiswaBaruView.SaveNextButtonClicked += _daftarSiswaBaruView_SaveNextButtonClicked;
            _daftarSiswaBaruView.NewDataButtonClicked += _daftarSiswaBaruView_NewDataButtonClicked;

        }

        private void _daftarSiswaBaruView_NewDataButtonClicked()
        {
            AssignNewViewModel();
            _daftarSiswaBaruView.ClearView();
            _daftarSiswaBaruView.ChangeEditable(true);
        }

        void p_View_Load(object sender, EventArgs e)
        {
            _daftarSiswaBaruView.KelasDataSource = GetListKelasDatasource();
            AssignNewViewModel();
            _daftarSiswaBaruView.ClearView();
            _daftarSiswaBaruView.SetBinding();
            
        }

        private void AssignNewViewModel()
        {
            var refTA = (ValueList)_daftarSiswaBaruProcess.GetTahunAjaran(DateTime.Now).DataResult;
            _daftarSiswaBaruView.ViewModel = new DaftarSiswaBaruModel
            {
                TahunAjaran_Id = (int)refTA.Id,
                TahunAjaran_Nama = (string)refTA.Value,
                Mulai_Tanggal = DateTime.Now
            };
        }

        private object GetListKelasDatasource()
        {
            return _daftarSiswaBaruProcess.GetAllKelas().DataResult;
        }

        void _daftarSiswaBaruView_SaveNextButtonClicked()
        {
            SaveData();
            AssignNewViewModel();
            _daftarSiswaBaruView.ChangeEditable(true);
            _daftarSiswaBaruView.ClearView();
        }

        
        void SaveData()
        {
            var model = _daftarSiswaBaruView.ViewModel;
            ProcessResult procResult = _daftarSiswaBaruProcess.TambahSiswaBaru(model);

            if (procResult.ProcessException != null)
            {
                _daftarSiswaBaruView.ShowErrorMessage(procResult.ProcessException.Message, AppResource.CAP_PROCESS_ERROR);
            }
            else
            {
                _daftarSiswaBaruView.ShowInformation(AppResource.MSG_DATA_SAVED_SUCCESSFULLY, AppResource.CAP_PROCESS_INFORMATION);
            }
        }

        void _daftarSiswaBaruView_SaveButtonClicked()
        {
            SaveData();
            _daftarSiswaBaruView.ChangeEditable(false);
        }

        void _daftarSiswaBaruView_JemputanButtonClicked()
        {
            throw new NotImplementedException();
        }

        void _daftarSiswaBaruView_PotonganButtonClicked()
        {
            throw new NotImplementedException();
        }

       
    }
}
