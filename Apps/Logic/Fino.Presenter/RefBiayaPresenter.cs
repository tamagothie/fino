﻿using Fino.BusinessLogic;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Model;
using Fino.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fino.Presenter
{
    public class RefBiayaPresenter
    {
        private readonly IRefBiayaView _KonfigurasiBiayaView;
        private readonly IKonfigurasiBiayaProcess _KonfigurasiBiayaProcess;

        public RefBiayaPresenter(IRefBiayaView p_KonfigurasiBiayaView, 
            IKonfigurasiBiayaProcess p_KonfigurasiBiayaProcess)
        {
            _KonfigurasiBiayaView = p_KonfigurasiBiayaView;
            _KonfigurasiBiayaProcess = p_KonfigurasiBiayaProcess;

            _KonfigurasiBiayaView.Load += KonfigurasiBiayaFormLoad;
            _KonfigurasiBiayaView.GvRefBiayaDoubleClick += _KonfigurasiBiayaView_GvKonfigurasiBiayaDoubleClick;
            _KonfigurasiBiayaView.BtnTambahClick += _KonfigurasiBiayaView_BtnTambahClick;
            _KonfigurasiBiayaView.BtnTutupClick += _KonfigurasiBiayaView_BtnTutupClick;
            _KonfigurasiBiayaView.ReloadData += _KonfigurasiBiayaView_ReloadData;
            _KonfigurasiBiayaView.BtnHapusClick += _KonfigurasiBiayaView_BtnHapusClick;
        }

        void _KonfigurasiBiayaView_BtnHapusClick()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                            () => _KonfigurasiBiayaProcess.DeleteRefBiaya(_KonfigurasiBiayaView.GetSelectedKonfigurasiBiaya())),
                            AppResource.MSG_DELETE_REF_BIAYA);

            if (result.IsSucess)
            {
                LoadData();
            }
            else
            {
                MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }    
        }

        void _KonfigurasiBiayaView_ReloadData()
        {
            LoadData();
        }

        void _KonfigurasiBiayaView_BtnTutupClick()
        {
            _KonfigurasiBiayaView.Close();
        }

        void _KonfigurasiBiayaView_BtnTambahClick()
        {
            _KonfigurasiBiayaView.ShowTambahRefBiayaView();
        }

        void _KonfigurasiBiayaView_GvKonfigurasiBiayaDoubleClick()
        {
            _KonfigurasiBiayaView.ShowEditRefBiayaView();
        }

        public void KonfigurasiBiayaFormLoad(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            ProcessResult result = TaskProgress.GetDataResult(new Task<ProcessResult>(
                () => _KonfigurasiBiayaProcess.GetRefBiaya()),
                AppResource.MSG_LOADING_REF_BIAYA);

            if (result.IsSucess)
            {
                List<RefBiayaModel> data = result.DataResult as List<RefBiayaModel>;

                _KonfigurasiBiayaView.SetData(data);
                _KonfigurasiBiayaView.RefreshDataGrid();
            }
            else
            {
                MessageBox.Show(AppResource.CAP_PROCESS_ERROR, AppResource.ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
