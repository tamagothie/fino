﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IMainFormView : IView
    {
        event Action ManageSiswaMenuClicked;
        event Action KonfigurasiBiayaClicked;
        event Action PendaftaranSiswaBaruClicked;
        // event Action ShowGrupBiayaClicked;
        event Action ReportRPPClicked;

        void ShowManageSiswaView();
        void ShowKonfigurasiBiayaView();
        bool CheckLogonStatus();
        void ShowDaftarSiswaBaruView();
        void ShowGrupBiayaView();
        void ShowReportRPPView();
    }
}
