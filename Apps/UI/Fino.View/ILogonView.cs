﻿using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface ILogonView : IView
    {           
        event Action OkButtonClicked;
        event Action CancelButtonClicked;

        UserModel User { get; set; }
        string Username { get; set; }
        string Password { get; set; }
        bool LogonResult { get; set; }

        void SetBinding();
    }
}
