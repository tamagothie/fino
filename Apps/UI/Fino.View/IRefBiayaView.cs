﻿using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IRefBiayaView : IView
    {
        event Action GvRefBiayaDoubleClick;
        event Action BtnTambahClick;
        event Action BtnHapusClick;
        event Action BtnTutupClick;
        event Action ReloadData;

        void SetData(List<RefBiayaModel> p_ListKonfigurasiBiaya);
        void RefreshDataGrid();
        void ShowEditRefBiayaView();
        void ShowTambahRefBiayaView();
        void ReloadDataEvent();
        RefBiayaModel GetSelectedKonfigurasiBiaya();
    }
}
