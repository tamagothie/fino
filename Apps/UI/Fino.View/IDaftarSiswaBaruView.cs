﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Lib.Core;
using Fino.Model;

namespace Fino.View
{
    public interface IDaftarSiswaBaruView: IView
    {
        event Action SaveButtonClicked;
        event Action SaveNextButtonClicked;
        event Action NewDataButtonClicked;
        event Action PotonganButtonClicked;
        event Action KateringButtonClicked;
        event Action JemputanButtonClicked;

        string NoInduk { get; set; }
        string NamaSiswa { get; set; }
        bool JenisKelamin { get; set; }
        int KelasId { get;set; }
        DateTime MulaiTanggal { get; set; }
        int TahunAjaranId { get; set; }
        string TahunAjaranName { get; set; }

        DaftarSiswaBaruModel ViewModel { get; set; }
        object KelasDataSource { get; set; }

        void SetBinding();
        void ClearView();
        void ChangeEditable(bool enabled);
        void ShowErrorMessage(string message, string caption);
        void ShowInformation(string message, string caption);

    }
}
