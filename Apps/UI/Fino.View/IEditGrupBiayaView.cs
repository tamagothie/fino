﻿using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IEditGrupBiayaView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;

        GrupBiayaModel SelectedGrupBiaya { get; }
        IGrupBiayaView GrupBiayaView { get; set; }

        string Nama { get; set; }
        string Kode { get; set; }

        void ShowForm(GrupBiayaModel p_KonfigurasiBiaya);
        void SetBinding();
        void ClearData();
        void RaiseBtnSimpanClicked();
        void RaiseBtnBatalClicked();
    }
}
