﻿using Fino.Lib.Core;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.View
{
    public interface IEditRefBiayaView : IView
    {
        event Action BtnBatalClicked;
        event Action BtnSimpanClicked;
        event Action CmbRepetisiValueChanged;

        RefBiayaModel SelectedKonfigurasiBiaya { get; }
        IRefBiayaView KonfigurasiBiayaView { get; set; }

        string NamaBiaya { get; set; }
        string KodeBiaya { get; set; }
        int Repetisi { get; set; }
        int TanggalJatuhTempo { get; set; }
        int BulanJatuhTempo { get; set; }
        DateTime MulaiTanggal { get; set; }
        bool CmbJTBulanEnabled { get; set; }

        void SetBinding();
        void ClearData();        
    }
}
