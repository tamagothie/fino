﻿namespace Fino.Cient
{
    partial class MainFormView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuListView = new System.Windows.Forms.ListView();
            this.panelKiri = new System.Windows.Forms.Panel();
            this.panelKiri.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuListView
            // 
            this.menuListView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.menuListView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.menuListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.menuListView.FullRowSelect = true;
            this.menuListView.Location = new System.Drawing.Point(0, 0);
            this.menuListView.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.menuListView.MultiSelect = false;
            this.menuListView.Name = "menuListView";
            this.menuListView.Size = new System.Drawing.Size(290, 486);
            this.menuListView.TabIndex = 0;
            this.menuListView.TileSize = new System.Drawing.Size(225, 40);
            this.menuListView.UseCompatibleStateImageBehavior = false;
            this.menuListView.View = System.Windows.Forms.View.Tile;
            // 
            // panelKiri
            // 
            this.panelKiri.BackColor = System.Drawing.Color.White;
            this.panelKiri.Controls.Add(this.menuListView);
            this.panelKiri.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelKiri.Location = new System.Drawing.Point(0, 0);
            this.panelKiri.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.panelKiri.Name = "panelKiri";
            this.panelKiri.Size = new System.Drawing.Size(290, 486);
            this.panelKiri.TabIndex = 2;
            // 
            // MainFormView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(914, 486);
            this.Controls.Add(this.panelKiri);
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "MainFormView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Administrasi Sekolah";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.panelKiri.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView menuListView;
        private System.Windows.Forms.Panel panelKiri;
    }
}