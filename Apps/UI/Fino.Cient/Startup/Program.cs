﻿using Fino.Cient.Configuration;
using Fino.Cient.Startup;
using Fino.Lib.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace Fino.Cient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!SingleInstance.Start())
            {
                SingleInstance.ShowFirstInstance();
                return;
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ViewLocator.Instance.Initialise();
            MapperConfiguration.Instance.SetupMapping();

            ViewLocator.Instance.LogonView.ShowDialog();

            if (ViewLocator.Instance.LogonView.LogonResult)
            {
                Application.Run(ViewLocator.Instance.MainFormView);
            }            

            SingleInstance.Stop();
        }
    }
}
