﻿using Fino.View;
using Fino.Presenter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.BusinessLogic;
using Fino.Cient.Report;

namespace Fino.Cient.Startup
{
    public class ViewLocator
    {
        EditRefBiayaView _editPosBiayaView;
        EditGrupBiayaView _editGrupBiayaView;
        RefBiayaView _posBiayaView;
        GrupBiayaView _grupBiayaView;
        KonfigurasiBiayaView _konfigurasiBiayaView;
        RepRekapPPView _reportRekapPPView;

        public LogonView LogonView { get; private set; }
        public MainFormView MainFormView { get; private set; }

        public RepRekapPPView ReportRekapPPView
        {
            get
            {
                if ((_reportRekapPPView == null) || (_reportRekapPPView != null && _reportRekapPPView.IsDisposed))
                {
                    _reportRekapPPView = CreateNewRekapPPView();
                    return _reportRekapPPView;
                }
                else
                {
                    _reportRekapPPView.BringToFront();
                    return _reportRekapPPView;
                }
            }
        }

        public KonfigurasiBiayaView KonfigurasiBiayaView
        {
            get
            {
                if ((_konfigurasiBiayaView == null) || (_konfigurasiBiayaView != null && _konfigurasiBiayaView.IsDisposed))
                {
                    _konfigurasiBiayaView = CreateNewKonfigurasiBiayaView();
                    return _konfigurasiBiayaView;
                }
                else
                {
                    _konfigurasiBiayaView.BringToFront();
                    return _konfigurasiBiayaView;
                }
            }
        }

        public RefBiayaView PosBiayaView 
        { 
            get
            {
                if ((_posBiayaView == null) || (_posBiayaView != null && _posBiayaView.IsDisposed))
                {
                    _posBiayaView = CreateNewPosBiayaView();
                    return _posBiayaView;
                }
                else
                {
                    _posBiayaView.BringToFront();
                    return _posBiayaView;
                }
            }
        }

        public EditRefBiayaView EditPosBiayaView
        {
            get
            {
                if ((_editPosBiayaView == null) || (_editPosBiayaView != null && _editPosBiayaView.IsDisposed))
                {
                    _editPosBiayaView = CreateNewEditPosBiaya();
                    return _editPosBiayaView;
                }
                else
                {
                    _editPosBiayaView.BringToFront();
                    return _editPosBiayaView;
                }
            }
        }

        public EditGrupBiayaView EditGrupBiayaView
        {
            get
            {
                if ((_editGrupBiayaView == null) || (_editGrupBiayaView != null && _editGrupBiayaView.IsDisposed))
                {
                    _editGrupBiayaView = CreateNewEditGrupBiaya();
                    return _editGrupBiayaView;
                }
                else
                {
                    _editGrupBiayaView.BringToFront();
                    return _editGrupBiayaView;
                }
            }
        }

        public GrupBiayaView GrupBiayaView
        {
            get
            {
                if ((_grupBiayaView == null) || (_grupBiayaView != null && _grupBiayaView.IsDisposed))
                {
                    _grupBiayaView = CreateNewGrupBiayaView();
                    return _grupBiayaView;
                }
                else
                {
                    _grupBiayaView.BringToFront();
                    return _grupBiayaView;
                }
            }
        }

        #region Singleton
        private static readonly Lazy<ViewLocator> lazy = new Lazy<ViewLocator>(() => new ViewLocator());
    
        public static ViewLocator Instance { get { return lazy.Value; } }

        private ViewLocator() { }
        #endregion

        public void Initialise()
        {
            RegisterLogonView();
            RegisterMainView();
        }

        private void RegisterMainView()
        {
            MainFormView = new MainFormView();
            MainFormPresenter mainFormPresenter = new MainFormPresenter(MainFormView);
        }

        private void RegisterLogonView()
        {
            LogonView = new LogonView();
            IUserLogonProcess userLogonProcess = new UserLogonProcess();

            LogonPresenter logonPresenter = new LogonPresenter(LogonView, userLogonProcess);
        }

        private EditRefBiayaView CreateNewEditPosBiaya()
        {
            EditRefBiayaView view = new EditRefBiayaView();
            KonfigurasiBiayaProcess process = new KonfigurasiBiayaProcess();
            view.MdiParent = MainFormView;

            EditRefBiayaPresenter presenter = new EditRefBiayaPresenter(process, view);

            return view;
        }

        private EditGrupBiayaView CreateNewEditGrupBiaya()
        {
            EditGrupBiayaView editGrupBiayaView = new EditGrupBiayaView();
            KonfigurasiBiayaProcess process = new KonfigurasiBiayaProcess();
            editGrupBiayaView.MdiParent = MainFormView;

            EditGrupBiayaPresenter presenter = new EditGrupBiayaPresenter(editGrupBiayaView, process);

            return editGrupBiayaView;
        }

        private RefBiayaView CreateNewPosBiayaView()
        {
            RefBiayaView kbv = new RefBiayaView();            
            KonfigurasiBiayaProcess konfigurasiBiayaProcess = new KonfigurasiBiayaProcess();
            kbv.MdiParent = MainFormView;

            RefBiayaPresenter konfigurasiBiayaPresenter = new RefBiayaPresenter(kbv, konfigurasiBiayaProcess);

            return kbv;
        }

        private Cient.GrupBiayaView CreateNewGrupBiayaView()
        {
            GrupBiayaView view = new GrupBiayaView();
            KonfigurasiBiayaProcess konfigurasiBiayaProcess = new KonfigurasiBiayaProcess();
            view.MdiParent = MainFormView;

            GrupBiayaPresenter konfigurasiBiayaPresenter = new GrupBiayaPresenter(view, konfigurasiBiayaProcess);

            return view;
        }

        private Cient.KonfigurasiBiayaView CreateNewKonfigurasiBiayaView()
        {
            KonfigurasiBiayaView view = new KonfigurasiBiayaView();
            KonfigurasiBiayaProcess konfigurasiBiayaProcess = new KonfigurasiBiayaProcess();
            view.MdiParent = MainFormView;

            KonfigurasiBiayaPresenter konfigurasiBiayaPresenter = new KonfigurasiBiayaPresenter(view, konfigurasiBiayaProcess);

            return view;
        }

        private RepRekapPPView CreateNewRekapPPView()
        {
            RepRekapPPView view = new RepRekapPPView();
            PosBiayaProcess process = new PosBiayaProcess();
            view.MdiParent = MainFormView;

            RekapPPPresenter presenter = new RekapPPPresenter(view, process);

            return view;
        }
    }
}
