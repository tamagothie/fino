﻿using Fino.BusinessLogic;
using Fino.Cient.Startup;
using Fino.Lib.Core;
using Fino.Lib.Win32;
using Fino.Presenter;
using Fino.View;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Fino.Cient
{
    public partial class MainFormView : BaseForm, IMainFormView
    {
        public event Action ManageSiswaMenuClicked;
        public event Action KonfigurasiBiayaClicked;
        public event Action PendaftaranSiswaBaruClicked;
        //public event Action ShowGrupBiayaClicked;
        public event Action ReportRPPClicked;

        public MainFormView()
        {
            InitializeComponent();

            InitializeMenuListView();
        }

        public void ShowManageSiswaView()
        {
            ManageSiswaView msv = new ManageSiswaView();
            msv.MdiParent = this;
            ManageSiswaPresenter manageSiswaPresenter = new ManageSiswaPresenter(msv);

            msv.Show();
        }

        public bool CheckLogonStatus()
        {
            return ViewLocator.Instance.LogonView.LogonResult;
        }

        public void ShowKonfigurasiBiayaView()
        {
            ViewLocator.Instance.PosBiayaView.Show();
        }

        public void ShowGrupBiayaView()
        {
            ViewLocator.Instance.GrupBiayaView.Show();
        }

        public void ShowDaftarSiswaBaruView()
        {
            DaftarSiswaBaruView view = new DaftarSiswaBaruView();
            view.MdiParent = this;
            IDaftarSiswaBaruProcess daftarSiswaBaruProcess = new DaftarSiswaBaruProcess();

            DaftarSiswaBaruPresenter DaftarSiswaBaruPresenter = new DaftarSiswaBaruPresenter(daftarSiswaBaruProcess, view);

            view.Show();
        }

        public void ShowReportRPPView()
        {
            ViewLocator.Instance.ReportRekapPPView.Show();
        }

        //    {
        //        ShowGrupBiayaClicked();
        //    }
        //}

        private void laporanPiutangDanPenerimaanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ReportRPPClicked != null)
            {
                ReportRPPClicked();
            }
        }
        private void InitializeMenuListView()
        {
            string menuImageLocation = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\Images\Menus";

            foreach (var header in Constants.Instance.MenuGroupHeader)
            {
                menuListView.Groups.Add(header, header);
            }

            ImageList imgList = new ImageList();
            foreach (Constants.MenuEnums menuEnum in Enum.GetValues(typeof(Constants.MenuEnums)))
            {
                string imageFilepath = Path.Combine(menuImageLocation, menuEnum.ToString() + ".png");
                if (File.Exists(imageFilepath))
                {
                    Image img = Image.FromFile(imageFilepath);
                    imgList.Images.Add(menuEnum.ToString(), img);
                }
            }

            menuListView.LargeImageList = imgList;
            menuListView.SmallImageList = imgList;

            ListViewItem pendaftaranSiswaBaruItem = new ListViewItem("Pendaftaran Siswa Baru", Constants.MenuEnums.PendaftaranSiswaBaru.ToString());
            pendaftaranSiswaBaruItem.Tag = Constants.MenuEnums.PendaftaranSiswaBaru;
            ListViewItem daftarUlangItem = new ListViewItem("Daftar Ulang", Constants.MenuEnums.DaftarUlang.ToString());
            daftarUlangItem.Tag = Constants.MenuEnums.DaftarUlang;
            ListViewItem potonganBiayaItem = new ListViewItem("Potongan Biaya", Constants.MenuEnums.PotonganBiaya.ToString());
            potonganBiayaItem.Tag = Constants.MenuEnums.PotonganBiaya;
            ListViewItem jemputanItem = new ListViewItem("Jemputan", Constants.MenuEnums.Jemputan.ToString());
            jemputanItem.Tag = Constants.MenuEnums.Jemputan;
            ListViewItem kateringItem = new ListViewItem("Katering", Constants.MenuEnums.Katering.ToString());
            kateringItem.Tag = Constants.MenuEnums.Katering;

            menuListView.Items.Add(pendaftaranSiswaBaruItem);
            menuListView.Items.Add(daftarUlangItem);
            menuListView.Items.Add(potonganBiayaItem);
            menuListView.Items.Add(jemputanItem);
            menuListView.Items.Add(kateringItem);

            menuListView.Groups["Siswa"].Items.Add(pendaftaranSiswaBaruItem);
            menuListView.Groups["Siswa"].Items.Add(daftarUlangItem);
            menuListView.Groups["Siswa"].Items.Add(potonganBiayaItem);
            menuListView.Groups["Siswa"].Items.Add(jemputanItem);
            menuListView.Groups["Siswa"].Items.Add(kateringItem);

            ListViewItem jenisBiayaItem = new ListViewItem("Jenis Biaya", Constants.MenuEnums.JenisBiaya.ToString());
            jenisBiayaItem.Tag = Constants.MenuEnums.JenisBiaya;
            ListViewItem grupBiayaItem = new ListViewItem("Grup Biaya", Constants.MenuEnums.GrupBiaya.ToString());
            grupBiayaItem.Tag = Constants.MenuEnums.GrupBiaya;

            menuListView.Items.Add(jenisBiayaItem);
            menuListView.Items.Add(grupBiayaItem);

            menuListView.Groups["Biaya"].Items.Add(jenisBiayaItem);
            menuListView.Groups["Biaya"].Items.Add(grupBiayaItem);

            ListViewItem setoranItem = new ListViewItem("Setoran", Constants.MenuEnums.Setoran.ToString());
            setoranItem.Tag = Constants.MenuEnums.Setoran;
            ListViewItem penarikanItem = new ListViewItem("Penarikan", Constants.MenuEnums.Penarikan.ToString());
            penarikanItem.Tag = Constants.MenuEnums.Penarikan;
            ListViewItem laporanTabunganItem = new ListViewItem("Laporan", Constants.MenuEnums.LaporanTabungan.ToString());
            laporanTabunganItem.Tag = Constants.MenuEnums.LaporanTabungan;

            menuListView.Items.Add(setoranItem);
            menuListView.Items.Add(penarikanItem);
            menuListView.Items.Add(laporanTabunganItem);

            menuListView.Groups["Tabungan"].Items.Add(setoranItem);
            menuListView.Groups["Tabungan"].Items.Add(penarikanItem);
            menuListView.Groups["Tabungan"].Items.Add(laporanTabunganItem);

            ListViewItem kodeAkunItem = new ListViewItem("Kode Akun", Constants.MenuEnums.KodeAkun.ToString());
            kodeAkunItem.Tag = Constants.MenuEnums.KodeAkun;
            ListViewItem postingJurnalItem = new ListViewItem("Posting Jurnal", Constants.MenuEnums.PostingJurnal.ToString());
            postingJurnalItem.Tag = Constants.MenuEnums.PostingJurnal;
            ListViewItem postingBukuBesarItem = new ListViewItem("Posting Buku Besar", Constants.MenuEnums.PostingBukuBesar.ToString());
            postingBukuBesarItem.Tag = Constants.MenuEnums.PostingBukuBesar;
            ListViewItem laporanAkuntansiItem = new ListViewItem("Laporan", Constants.MenuEnums.LaporanAkuntansi.ToString());
            laporanAkuntansiItem.Tag = Constants.MenuEnums.LaporanAkuntansi;

            menuListView.Items.Add(kodeAkunItem);
            menuListView.Items.Add(postingJurnalItem);
            menuListView.Items.Add(postingBukuBesarItem);
            menuListView.Items.Add(laporanAkuntansiItem);

            menuListView.Groups["Akuntansi"].Items.Add(kodeAkunItem);
            menuListView.Groups["Akuntansi"].Items.Add(postingJurnalItem);
            menuListView.Groups["Akuntansi"].Items.Add(postingBukuBesarItem);
            menuListView.Groups["Akuntansi"].Items.Add(laporanAkuntansiItem);

            // ListViewItem laporanPiutangItem = new ListViewItem("Laporan Piutang dan Penerimaan", Constants.MenuEnums.LaporanPiutang.ToString());
            // laporanPiutangItem.Tag = Constants.MenuEnums.LaporanPiutang;
            ListViewItem ReportRPPItem = new ListViewItem("Laporan Penerimaan dan Piutang", Constants.MenuEnums.ReportRPP.ToString());
            ReportRPPItem.Tag = Constants.MenuEnums.ReportRPP;

            menuListView.Items.Add(ReportRPPItem);

            menuListView.Groups["Kas"].Items.Add(ReportRPPItem);

            menuListView.MouseClick += menuListView_MouseClick;
        }

        private void menuListView_MouseClick(object sender, MouseEventArgs e)
        {
            ListViewItem clickedItem = menuListView.GetItemAt(e.X, e.Y);

            if (clickedItem != null)
            {
                var tag = (Constants.MenuEnums)clickedItem.Tag;

                switch (tag)
                {
                    case 0:
                        if (PendaftaranSiswaBaruClicked != null)
                        {
                            PendaftaranSiswaBaruClicked();
                        }
                        break;
                    case Constants.MenuEnums.JenisBiaya:
                        if (KonfigurasiBiayaClicked != null)
                        {
                            KonfigurasiBiayaClicked();
                        }
                        break;
                    case Constants.MenuEnums.GrupBiaya:
                        ViewLocator.Instance.GrupBiayaView.Show();
                        break;
                    case Constants.MenuEnums.LaporanPiutang:
                        ViewLocator.Instance.ReportRekapPPView.Show();
                        break;
                    //}
                    //break;
                    case Constants.MenuEnums.ReportRPP:
                        if (ReportRPPClicked != null)
                        {
                            ReportRPPClicked();
                        }
                        break;
                }
            }
        }
    }
}
