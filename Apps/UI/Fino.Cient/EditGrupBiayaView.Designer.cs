﻿namespace Fino.Cient
{
    partial class EditGrupBiayaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblKodeBiaya = new System.Windows.Forms.Label();
            this.lblNamaGrupBiaya = new System.Windows.Forms.Label();
            this.txtKodeGrupBiaya = new System.Windows.Forms.TextBox();
            this.txtNamaGrupBiaya = new System.Windows.Forms.TextBox();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.btnBatal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblKodeBiaya
            // 
            this.lblKodeBiaya.AutoSize = true;
            this.lblKodeBiaya.Location = new System.Drawing.Point(64, 61);
            this.lblKodeBiaya.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblKodeBiaya.Name = "lblKodeBiaya";
            this.lblKodeBiaya.Size = new System.Drawing.Size(158, 20);
            this.lblKodeBiaya.TabIndex = 6;
            this.lblKodeBiaya.Text = "Kode grup biaya:";
            // 
            // lblNamaGrupBiaya
            // 
            this.lblNamaGrupBiaya.AutoSize = true;
            this.lblNamaGrupBiaya.Location = new System.Drawing.Point(57, 21);
            this.lblNamaGrupBiaya.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblNamaGrupBiaya.Name = "lblNamaGrupBiaya";
            this.lblNamaGrupBiaya.Size = new System.Drawing.Size(165, 20);
            this.lblNamaGrupBiaya.TabIndex = 7;
            this.lblNamaGrupBiaya.Text = "Nama grup biaya:";
            // 
            // txtKodeGrupBiaya
            // 
            this.txtKodeGrupBiaya.Location = new System.Drawing.Point(257, 58);
            this.txtKodeGrupBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtKodeGrupBiaya.MaxLength = 10;
            this.txtKodeGrupBiaya.Name = "txtKodeGrupBiaya";
            this.txtKodeGrupBiaya.Size = new System.Drawing.Size(196, 28);
            this.txtKodeGrupBiaya.TabIndex = 4;
            // 
            // txtNamaGrupBiaya
            // 
            this.txtNamaGrupBiaya.Location = new System.Drawing.Point(257, 18);
            this.txtNamaGrupBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtNamaGrupBiaya.MaxLength = 25;
            this.txtNamaGrupBiaya.Name = "txtNamaGrupBiaya";
            this.txtNamaGrupBiaya.Size = new System.Drawing.Size(275, 28);
            this.txtNamaGrupBiaya.TabIndex = 5;
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(268, 122);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(138, 35);
            this.btnSimpan.TabIndex = 8;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(418, 122);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(138, 35);
            this.btnBatal.TabIndex = 9;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // EditGrupBiayaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(571, 175);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.lblKodeBiaya);
            this.Controls.Add(this.lblNamaGrupBiaya);
            this.Controls.Add(this.txtKodeGrupBiaya);
            this.Controls.Add(this.txtNamaGrupBiaya);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "EditGrupBiayaView";
            this.Text = "Edit Grup Biaya";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblKodeBiaya;
        private System.Windows.Forms.Label lblNamaGrupBiaya;
        private System.Windows.Forms.TextBox txtKodeGrupBiaya;
        private System.Windows.Forms.TextBox txtNamaGrupBiaya;
        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
    }
}