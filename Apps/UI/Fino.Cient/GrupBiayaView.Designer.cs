﻿namespace Fino.Cient
{
    partial class GrupBiayaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnTambah = new System.Windows.Forms.Button();
            this.btnHapus = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnAturPosBiaya = new System.Windows.Forms.Button();
            this.gvGrupBiaya = new System.Windows.Forms.DataGridView();
            this.grupBiayaModelBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grupBiayaIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.codeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.namaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aktifDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.validationMessageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvGrupBiaya)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grupBiayaModelBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTambah
            // 
            this.btnTambah.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnTambah.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTambah.ForeColor = System.Drawing.Color.White;
            this.btnTambah.Location = new System.Drawing.Point(22, 18);
            this.btnTambah.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnTambah.Name = "btnTambah";
            this.btnTambah.Size = new System.Drawing.Size(162, 35);
            this.btnTambah.TabIndex = 0;
            this.btnTambah.Text = "Tambah";
            this.btnTambah.UseVisualStyleBackColor = false;
            this.btnTambah.Click += new System.EventHandler(this.btnTambah_Click);
            // 
            // btnHapus
            // 
            this.btnHapus.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnHapus.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnHapus.ForeColor = System.Drawing.Color.White;
            this.btnHapus.Location = new System.Drawing.Point(22, 63);
            this.btnHapus.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnHapus.Name = "btnHapus";
            this.btnHapus.Size = new System.Drawing.Size(162, 35);
            this.btnHapus.TabIndex = 1;
            this.btnHapus.Text = "Hapus";
            this.btnHapus.UseVisualStyleBackColor = false;
            this.btnHapus.Click += new System.EventHandler(this.btnHapus_Click);
            // 
            // btnClose
            // 
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Location = new System.Drawing.Point(22, 152);
            this.btnClose.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(162, 35);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Tutup";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pnlMenu
            // 
            this.pnlMenu.Controls.Add(this.btnAturPosBiaya);
            this.pnlMenu.Controls.Add(this.btnHapus);
            this.pnlMenu.Controls.Add(this.btnTambah);
            this.pnlMenu.Controls.Add(this.btnClose);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(206, 442);
            this.pnlMenu.TabIndex = 3;
            // 
            // btnAturPosBiaya
            // 
            this.btnAturPosBiaya.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnAturPosBiaya.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAturPosBiaya.ForeColor = System.Drawing.Color.White;
            this.btnAturPosBiaya.Location = new System.Drawing.Point(22, 108);
            this.btnAturPosBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnAturPosBiaya.Name = "btnAturPosBiaya";
            this.btnAturPosBiaya.Size = new System.Drawing.Size(162, 35);
            this.btnAturPosBiaya.TabIndex = 1;
            this.btnAturPosBiaya.Text = "Atur Pos Biaya";
            this.btnAturPosBiaya.UseVisualStyleBackColor = false;
            this.btnAturPosBiaya.Click += new System.EventHandler(this.btnAturPosBiaya_Click);
            // 
            // gvGrupBiaya
            // 
            this.gvGrupBiaya.AllowUserToAddRows = false;
            this.gvGrupBiaya.AllowUserToDeleteRows = false;
            this.gvGrupBiaya.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gvGrupBiaya.AutoGenerateColumns = false;
            this.gvGrupBiaya.BackgroundColor = System.Drawing.Color.White;
            this.gvGrupBiaya.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvGrupBiaya.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grupBiayaIdDataGridViewTextBoxColumn,
            this.codeDataGridViewTextBoxColumn,
            this.namaDataGridViewTextBoxColumn,
            this.aktifDataGridViewCheckBoxColumn,
            this.validationMessageDataGridViewTextBoxColumn});
            this.gvGrupBiaya.DataSource = this.grupBiayaModelBindingSource;
            this.gvGrupBiaya.Location = new System.Drawing.Point(218, 14);
            this.gvGrupBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.gvGrupBiaya.Name = "gvGrupBiaya";
            this.gvGrupBiaya.ReadOnly = true;
            this.gvGrupBiaya.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvGrupBiaya.Size = new System.Drawing.Size(612, 414);
            this.gvGrupBiaya.TabIndex = 4;
            // 
            // grupBiayaModelBindingSource
            // 
            this.grupBiayaModelBindingSource.DataSource = typeof(Fino.Model.GrupBiayaModel);
            // 
            // grupBiayaIdDataGridViewTextBoxColumn
            // 
            this.grupBiayaIdDataGridViewTextBoxColumn.DataPropertyName = "GrupBiayaId";
            this.grupBiayaIdDataGridViewTextBoxColumn.HeaderText = "GrupBiayaId";
            this.grupBiayaIdDataGridViewTextBoxColumn.Name = "grupBiayaIdDataGridViewTextBoxColumn";
            this.grupBiayaIdDataGridViewTextBoxColumn.ReadOnly = true;
            this.grupBiayaIdDataGridViewTextBoxColumn.Visible = false;
            // 
            // codeDataGridViewTextBoxColumn
            // 
            this.codeDataGridViewTextBoxColumn.DataPropertyName = "Code";
            this.codeDataGridViewTextBoxColumn.HeaderText = "Kode";
            this.codeDataGridViewTextBoxColumn.Name = "codeDataGridViewTextBoxColumn";
            this.codeDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // namaDataGridViewTextBoxColumn
            // 
            this.namaDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.namaDataGridViewTextBoxColumn.DataPropertyName = "Nama";
            this.namaDataGridViewTextBoxColumn.HeaderText = "Nama";
            this.namaDataGridViewTextBoxColumn.Name = "namaDataGridViewTextBoxColumn";
            this.namaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // aktifDataGridViewCheckBoxColumn
            // 
            this.aktifDataGridViewCheckBoxColumn.DataPropertyName = "Aktif";
            this.aktifDataGridViewCheckBoxColumn.HeaderText = "Aktif";
            this.aktifDataGridViewCheckBoxColumn.Name = "aktifDataGridViewCheckBoxColumn";
            this.aktifDataGridViewCheckBoxColumn.ReadOnly = true;
            this.aktifDataGridViewCheckBoxColumn.Visible = false;
            // 
            // validationMessageDataGridViewTextBoxColumn
            // 
            this.validationMessageDataGridViewTextBoxColumn.DataPropertyName = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.HeaderText = "ValidationMessage";
            this.validationMessageDataGridViewTextBoxColumn.Name = "validationMessageDataGridViewTextBoxColumn";
            this.validationMessageDataGridViewTextBoxColumn.ReadOnly = true;
            this.validationMessageDataGridViewTextBoxColumn.Visible = false;
            // 
            // GrupBiayaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(845, 442);
            this.Controls.Add(this.gvGrupBiaya);
            this.Controls.Add(this.pnlMenu);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "GrupBiayaView";
            this.Text = "Grup Biaya";
            this.pnlMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvGrupBiaya)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grupBiayaModelBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTambah;
        private System.Windows.Forms.Button btnHapus;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.DataGridView gvGrupBiaya;
        private System.Windows.Forms.Button btnAturPosBiaya;
        private System.Windows.Forms.BindingSource grupBiayaModelBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn grupBiayaIdDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn codeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn namaDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn aktifDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn validationMessageDataGridViewTextBoxColumn;
    }
}