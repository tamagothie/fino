﻿using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Cient.Configuration
{
    public class MapperConfiguration
    {
        #region Singleton
        private static readonly Lazy<MapperConfiguration> lazy = new Lazy<MapperConfiguration>(() => new MapperConfiguration());
    
        public static MapperConfiguration Instance { get { return lazy.Value; } }

        private MapperConfiguration() { }
        #endregion

        public void SetupMapping()
        {
            SetupKonfigurasiBiayaMapping();
            SetupUserMapping();
        }

        private void SetupUserMapping()
        {
            AutoMapper.Mapper.CreateMap<SysUser, UserModel>();
            AutoMapper.Mapper.CreateMap<UserModel, SysUser>();
        }

        private void SetupKonfigurasiBiayaMapping()
        {            
            AutoMapper.Mapper.CreateMap<RefBiaya, RefBiayaModel>()
                        .ForMember(dest => dest.Biaya_id, opts => opts.MapFrom(src => src.biaya_id))
                        .ForMember(dest => dest.Code, opts => opts.MapFrom(src => src.code))
                        .ForMember(dest => dest.Repetisi, opts => opts.MapFrom(src => src.repetisi))
                        .ForMember(dest => dest.Jt_tanggal, opts => opts.MapFrom(src => src.jt_tanggal))
                        .ForMember(dest => dest.Jt_bulan, opts => opts.MapFrom(src => src.jt_bulan))
                        .ForMember(dest => dest.Aktif, opts => opts.MapFrom(src => src.aktif))
                        .ForMember(dest => dest.Mulai_tanggal, opts => opts.MapFrom(src => src.mulai_tanggal))
                        .ForMember(dest => dest.Nama, opts => opts.MapFrom(src => src.nama));

            AutoMapper.Mapper.CreateMap<RefBiayaModel, RefBiaya>()
                        .ForMember(dest => dest.biaya_id, opts => opts.MapFrom(src => src.Biaya_id))
                        .ForMember(dest => dest.nama, opts => opts.MapFrom(src => src.Nama))
                        .ForMember(dest => dest.code, opts => opts.MapFrom(src => src.Code))
                        .ForMember(dest => dest.repetisi, opts => opts.MapFrom(src => src.Repetisi))
                        .ForMember(dest => dest.jt_tanggal, opts => opts.MapFrom(src => src.Jt_tanggal))
                        .ForMember(dest => dest.jt_bulan, opts => opts.MapFrom(src => src.Jt_bulan))
                        .ForMember(dest => dest.aktif, opts => opts.MapFrom(src => src.Aktif))
                        .ForMember(dest => dest.mulai_tanggal, opts => opts.MapFrom(src => src.Mulai_tanggal));

            AutoMapper.Mapper.CreateMap<RefGrupBiaya, GrupBiayaModel>()
                        .ForMember(dest => dest.GrupBiayaId, opts => opts.MapFrom(src => src.Grupbiaya_id))
                        .ForMember(dest => dest.Nama, opts => opts.MapFrom(src => src.nama))
                        .ForMember(dest => dest.Code, opts => opts.MapFrom(src => src.code))
                        .ForMember(dest => dest.Aktif, opts => opts.MapFrom(src => src.active));

            AutoMapper.Mapper.CreateMap<GrupBiayaModel, RefGrupBiaya>()
                        .ForMember(dest => dest.Grupbiaya_id, opts => opts.MapFrom(src => src.GrupBiayaId))
                        .ForMember(dest => dest.nama, opts => opts.MapFrom(src => src.Nama))
                        .ForMember(dest => dest.code, opts => opts.MapFrom(src => src.Code))
                        .ForMember(dest => dest.active, opts => opts.MapFrom(src => src.Aktif));
        }
    }
}
