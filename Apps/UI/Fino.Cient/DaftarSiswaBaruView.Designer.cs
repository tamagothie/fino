﻿namespace Fino.Cient
{
    partial class DaftarSiswaBaruView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPotongan = new System.Windows.Forms.Button();
            this.btnKatering = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnJemputan = new System.Windows.Forms.Button();
            this.btnSaveNew = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpMulaiTanggal = new System.Windows.Forms.DateTimePicker();
            this.cboKelas = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.rbPerempuan = new System.Windows.Forms.RadioButton();
            this.rbLaki = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamaSiswa = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTahunAjaran = new System.Windows.Forms.TextBox();
            this.txtNoInduk = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNew = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPotongan
            // 
            this.btnPotongan.Enabled = false;
            this.btnPotongan.Location = new System.Drawing.Point(29, 45);
            this.btnPotongan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnPotongan.Name = "btnPotongan";
            this.btnPotongan.Size = new System.Drawing.Size(116, 41);
            this.btnPotongan.TabIndex = 0;
            this.btnPotongan.Text = "Potongan";
            this.btnPotongan.UseVisualStyleBackColor = true;
            this.btnPotongan.Click += new System.EventHandler(this.btnPotongan_Click);
            // 
            // btnKatering
            // 
            this.btnKatering.Enabled = false;
            this.btnKatering.Location = new System.Drawing.Point(29, 144);
            this.btnKatering.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnKatering.Name = "btnKatering";
            this.btnKatering.Size = new System.Drawing.Size(116, 42);
            this.btnKatering.TabIndex = 2;
            this.btnKatering.Text = "Katering";
            this.btnKatering.UseVisualStyleBackColor = true;
            this.btnKatering.Click += new System.EventHandler(this.btnKatering_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(333, 478);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(116, 41);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Simpan";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.btnPotongan);
            this.groupBox1.Controls.Add(this.btnJemputan);
            this.groupBox1.Controls.Add(this.btnKatering);
            this.groupBox1.Location = new System.Drawing.Point(641, 26);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(175, 272);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pilihan";
            // 
            // btnJemputan
            // 
            this.btnJemputan.Enabled = false;
            this.btnJemputan.Location = new System.Drawing.Point(29, 94);
            this.btnJemputan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnJemputan.Name = "btnJemputan";
            this.btnJemputan.Size = new System.Drawing.Size(116, 42);
            this.btnJemputan.TabIndex = 1;
            this.btnJemputan.Text = "Jemputan";
            this.btnJemputan.UseVisualStyleBackColor = true;
            this.btnJemputan.Click += new System.EventHandler(this.btnJemputan_Click);
            // 
            // btnSaveNew
            // 
            this.btnSaveNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveNew.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSaveNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSaveNew.ForeColor = System.Drawing.Color.White;
            this.btnSaveNew.Location = new System.Drawing.Point(456, 478);
            this.btnSaveNew.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSaveNew.Name = "btnSaveNew";
            this.btnSaveNew.Size = new System.Drawing.Size(243, 41);
            this.btnSaveNew.TabIndex = 3;
            this.btnSaveNew.Text = "Simpan dan Data Baru";
            this.btnSaveNew.UseVisualStyleBackColor = false;
            this.btnSaveNew.Click += new System.EventHandler(this.btnSaveNew_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dtpMulaiTanggal);
            this.groupBox2.Controls.Add(this.cboKelas);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.rbPerempuan);
            this.groupBox2.Controls.Add(this.rbLaki);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtNamaSiswa);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtTahunAjaran);
            this.groupBox2.Controls.Add(this.txtNoInduk);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(16, 26);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(602, 272);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data Siswa Baru";
            // 
            // dtpMulaiTanggal
            // 
            this.dtpMulaiTanggal.CustomFormat = "dd-MMM-yyyy";
            this.dtpMulaiTanggal.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpMulaiTanggal.Location = new System.Drawing.Point(188, 185);
            this.dtpMulaiTanggal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpMulaiTanggal.Name = "dtpMulaiTanggal";
            this.dtpMulaiTanggal.Size = new System.Drawing.Size(169, 28);
            this.dtpMulaiTanggal.TabIndex = 10;
            // 
            // cboKelas
            // 
            this.cboKelas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboKelas.FormattingEnabled = true;
            this.cboKelas.Location = new System.Drawing.Point(188, 148);
            this.cboKelas.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cboKelas.Name = "cboKelas";
            this.cboKelas.Size = new System.Drawing.Size(252, 28);
            this.cboKelas.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(33, 226);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(132, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Tahun Ajaran:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 192);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Mulai Tanggal:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(110, 151);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 20);
            this.label5.TabIndex = 7;
            this.label5.Text = "Kelas:";
            // 
            // rbPerempuan
            // 
            this.rbPerempuan.AutoSize = true;
            this.rbPerempuan.Location = new System.Drawing.Point(309, 114);
            this.rbPerempuan.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbPerempuan.Name = "rbPerempuan";
            this.rbPerempuan.Size = new System.Drawing.Size(127, 24);
            this.rbPerempuan.TabIndex = 6;
            this.rbPerempuan.TabStop = true;
            this.rbPerempuan.Text = "Perempuan";
            this.rbPerempuan.UseVisualStyleBackColor = true;
            // 
            // rbLaki
            // 
            this.rbLaki.AutoSize = true;
            this.rbLaki.Location = new System.Drawing.Point(188, 114);
            this.rbLaki.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.rbLaki.Name = "rbLaki";
            this.rbLaki.Size = new System.Drawing.Size(102, 24);
            this.rbLaki.TabIndex = 5;
            this.rbLaki.TabStop = true;
            this.rbLaki.Text = "Laki-laki";
            this.rbLaki.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(87, 116);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Kelamin:";
            // 
            // txtNamaSiswa
            // 
            this.txtNamaSiswa.Location = new System.Drawing.Point(188, 76);
            this.txtNamaSiswa.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNamaSiswa.MaxLength = 25;
            this.txtNamaSiswa.Name = "txtNamaSiswa";
            this.txtNamaSiswa.Size = new System.Drawing.Size(252, 28);
            this.txtNamaSiswa.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nama Siswa:";
            // 
            // txtTahunAjaran
            // 
            this.txtTahunAjaran.Location = new System.Drawing.Point(188, 222);
            this.txtTahunAjaran.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTahunAjaran.MaxLength = 10;
            this.txtTahunAjaran.Name = "txtTahunAjaran";
            this.txtTahunAjaran.ReadOnly = true;
            this.txtTahunAjaran.Size = new System.Drawing.Size(377, 28);
            this.txtTahunAjaran.TabIndex = 12;
            // 
            // txtNoInduk
            // 
            this.txtNoInduk.Location = new System.Drawing.Point(188, 39);
            this.txtNoInduk.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtNoInduk.MaxLength = 10;
            this.txtNoInduk.Name = "txtNoInduk";
            this.txtNoInduk.Size = new System.Drawing.Size(169, 28);
            this.txtNoInduk.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 42);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "No Induk:";
            // 
            // btnNew
            // 
            this.btnNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNew.Location = new System.Drawing.Point(708, 478);
            this.btnNew.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(136, 41);
            this.btnNew.TabIndex = 4;
            this.btnNew.Text = "Data Baru";
            this.btnNew.UseVisualStyleBackColor = true;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // DaftarSiswaBaruView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(861, 534);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnSaveNew);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btnSave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "DaftarSiswaBaruView";
            this.Text = "Pendaftaran Siswa Baru";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPotongan;
        private System.Windows.Forms.Button btnKatering;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSaveNew;
        private System.Windows.Forms.Button btnJemputan;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtpMulaiTanggal;
        private System.Windows.Forms.ComboBox cboKelas;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RadioButton rbPerempuan;
        private System.Windows.Forms.RadioButton rbLaki;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNamaSiswa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTahunAjaran;
        private System.Windows.Forms.TextBox txtNoInduk;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnNew;
    }
}