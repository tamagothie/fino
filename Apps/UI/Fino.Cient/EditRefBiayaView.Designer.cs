﻿namespace Fino.Cient
{
    partial class EditRefBiayaView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNamaBiaya = new System.Windows.Forms.TextBox();
            this.txtKodeBiaya = new System.Windows.Forms.TextBox();
            this.txtTanggalJatuhTempo = new System.Windows.Forms.TextBox();
            this.dtTanggalMulai = new System.Windows.Forms.DateTimePicker();
            this.cmbBulanJatuhTempo = new System.Windows.Forms.ComboBox();
            this.lblNamaBiaya = new System.Windows.Forms.Label();
            this.lblKodeBiaya = new System.Windows.Forms.Label();
            this.lblRepetisi = new System.Windows.Forms.Label();
            this.lblTanggalJatuhTempo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnBatal = new System.Windows.Forms.Button();
            this.btnSimpan = new System.Windows.Forms.Button();
            this.cmbRepetisi = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtNamaBiaya
            // 
            this.txtNamaBiaya.Location = new System.Drawing.Point(239, 19);
            this.txtNamaBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtNamaBiaya.MaxLength = 25;
            this.txtNamaBiaya.Name = "txtNamaBiaya";
            this.txtNamaBiaya.Size = new System.Drawing.Size(219, 28);
            this.txtNamaBiaya.TabIndex = 0;
            // 
            // txtKodeBiaya
            // 
            this.txtKodeBiaya.Location = new System.Drawing.Point(239, 59);
            this.txtKodeBiaya.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtKodeBiaya.MaxLength = 10;
            this.txtKodeBiaya.Name = "txtKodeBiaya";
            this.txtKodeBiaya.Size = new System.Drawing.Size(219, 28);
            this.txtKodeBiaya.TabIndex = 1;
            // 
            // txtTanggalJatuhTempo
            // 
            this.txtTanggalJatuhTempo.Location = new System.Drawing.Point(239, 139);
            this.txtTanggalJatuhTempo.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.txtTanggalJatuhTempo.Name = "txtTanggalJatuhTempo";
            this.txtTanggalJatuhTempo.Size = new System.Drawing.Size(68, 28);
            this.txtTanggalJatuhTempo.TabIndex = 3;
            // 
            // dtTanggalMulai
            // 
            this.dtTanggalMulai.CustomFormat = "dd-MMM-yyyy";
            this.dtTanggalMulai.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtTanggalMulai.Location = new System.Drawing.Point(239, 223);
            this.dtTanggalMulai.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.dtTanggalMulai.Name = "dtTanggalMulai";
            this.dtTanggalMulai.Size = new System.Drawing.Size(219, 28);
            this.dtTanggalMulai.TabIndex = 5;
            // 
            // cmbBulanJatuhTempo
            // 
            this.cmbBulanJatuhTempo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBulanJatuhTempo.FormattingEnabled = true;
            this.cmbBulanJatuhTempo.Location = new System.Drawing.Point(239, 181);
            this.cmbBulanJatuhTempo.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.cmbBulanJatuhTempo.Name = "cmbBulanJatuhTempo";
            this.cmbBulanJatuhTempo.Size = new System.Drawing.Size(219, 28);
            this.cmbBulanJatuhTempo.TabIndex = 4;
            // 
            // lblNamaBiaya
            // 
            this.lblNamaBiaya.AutoSize = true;
            this.lblNamaBiaya.Location = new System.Drawing.Point(108, 22);
            this.lblNamaBiaya.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblNamaBiaya.Name = "lblNamaBiaya";
            this.lblNamaBiaya.Size = new System.Drawing.Size(119, 20);
            this.lblNamaBiaya.TabIndex = 3;
            this.lblNamaBiaya.Text = "Nama biaya:";
            // 
            // lblKodeBiaya
            // 
            this.lblKodeBiaya.AutoSize = true;
            this.lblKodeBiaya.Location = new System.Drawing.Point(115, 62);
            this.lblKodeBiaya.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblKodeBiaya.Name = "lblKodeBiaya";
            this.lblKodeBiaya.Size = new System.Drawing.Size(112, 20);
            this.lblKodeBiaya.TabIndex = 3;
            this.lblKodeBiaya.Text = "Kode biaya:";
            // 
            // lblRepetisi
            // 
            this.lblRepetisi.AutoSize = true;
            this.lblRepetisi.Location = new System.Drawing.Point(141, 102);
            this.lblRepetisi.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblRepetisi.Name = "lblRepetisi";
            this.lblRepetisi.Size = new System.Drawing.Size(86, 20);
            this.lblRepetisi.TabIndex = 3;
            this.lblRepetisi.Text = "Repetisi:";
            // 
            // lblTanggalJatuhTempo
            // 
            this.lblTanggalJatuhTempo.AutoSize = true;
            this.lblTanggalJatuhTempo.Location = new System.Drawing.Point(28, 142);
            this.lblTanggalJatuhTempo.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblTanggalJatuhTempo.Name = "lblTanggalJatuhTempo";
            this.lblTanggalJatuhTempo.Size = new System.Drawing.Size(199, 20);
            this.lblTanggalJatuhTempo.TabIndex = 3;
            this.lblTanggalJatuhTempo.Text = "Tanggal Jatuh tempo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 184);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Bulan Jatuh tempo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(91, 228);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tanggal Mulai:";
            // 
            // btnBatal
            // 
            this.btnBatal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBatal.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBatal.Location = new System.Drawing.Point(320, 302);
            this.btnBatal.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnBatal.Name = "btnBatal";
            this.btnBatal.Size = new System.Drawing.Size(138, 35);
            this.btnBatal.TabIndex = 7;
            this.btnBatal.Text = "Batal";
            this.btnBatal.UseVisualStyleBackColor = true;
            this.btnBatal.Click += new System.EventHandler(this.btnBatal_Click);
            // 
            // btnSimpan
            // 
            this.btnSimpan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSimpan.BackColor = System.Drawing.Color.RoyalBlue;
            this.btnSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSimpan.ForeColor = System.Drawing.Color.White;
            this.btnSimpan.Location = new System.Drawing.Point(171, 302);
            this.btnSimpan.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.btnSimpan.Name = "btnSimpan";
            this.btnSimpan.Size = new System.Drawing.Size(138, 35);
            this.btnSimpan.TabIndex = 6;
            this.btnSimpan.Text = "Simpan";
            this.btnSimpan.UseVisualStyleBackColor = false;
            this.btnSimpan.Click += new System.EventHandler(this.btnSimpan_Click);
            // 
            // cmbRepetisi
            // 
            this.cmbRepetisi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRepetisi.FormattingEnabled = true;
            this.cmbRepetisi.Location = new System.Drawing.Point(239, 99);
            this.cmbRepetisi.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.cmbRepetisi.Name = "cmbRepetisi";
            this.cmbRepetisi.Size = new System.Drawing.Size(219, 28);
            this.cmbRepetisi.TabIndex = 2;
            this.cmbRepetisi.SelectedValueChanged += new System.EventHandler(this.cmbRepetisi_SelectedValueChanged);
            // 
            // EditRefBiayaView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(526, 351);
            this.Controls.Add(this.btnSimpan);
            this.Controls.Add(this.btnBatal);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblTanggalJatuhTempo);
            this.Controls.Add(this.lblRepetisi);
            this.Controls.Add(this.lblKodeBiaya);
            this.Controls.Add(this.lblNamaBiaya);
            this.Controls.Add(this.cmbRepetisi);
            this.Controls.Add(this.cmbBulanJatuhTempo);
            this.Controls.Add(this.dtTanggalMulai);
            this.Controls.Add(this.txtTanggalJatuhTempo);
            this.Controls.Add(this.txtKodeBiaya);
            this.Controls.Add(this.txtNamaBiaya);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "EditRefBiayaView";
            this.Text = "Ubah Konfigurasi Biaya";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSimpan;
        private System.Windows.Forms.Button btnBatal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblTanggalJatuhTempo;
        private System.Windows.Forms.Label lblRepetisi;
        private System.Windows.Forms.Label lblKodeBiaya;
        private System.Windows.Forms.Label lblNamaBiaya;
        private System.Windows.Forms.ComboBox cmbBulanJatuhTempo;
        private System.Windows.Forms.DateTimePicker dtTanggalMulai;
        private System.Windows.Forms.TextBox txtTanggalJatuhTempo;
        private System.Windows.Forms.TextBox txtKodeBiaya;
        private System.Windows.Forms.TextBox txtNamaBiaya;
        private System.Windows.Forms.ComboBox cmbRepetisi;
    }
}