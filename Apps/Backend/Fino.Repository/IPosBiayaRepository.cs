﻿using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public interface IPosBiayaRepository : IDisposable
    {
        List<RekapitulasiPiutangDanPenerimaanModel> GetRekapitulasiPiutangDanPenerimaan(DateTime p_FromDate, DateTime p_ToDate);
        void GeneratePosBiaya(FinoDBContext context, DateTime pStart, DateTime pEnd, int pOption, RefBiaya pBiaya, RefSiswa pSiswa);
    }
}
