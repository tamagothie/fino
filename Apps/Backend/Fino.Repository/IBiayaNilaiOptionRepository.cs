﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public interface IBiayaNilaiOptionRepository
    {
        int GetNilaiBiaya(FinoDBContext context, RefBiaya pBiaya, int pOption);
    }
}
