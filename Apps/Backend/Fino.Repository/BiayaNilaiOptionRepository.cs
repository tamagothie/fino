﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;

namespace Fino.Repository
{
    public class BiayaNilaiOptionRepository : IBiayaNilaiOptionRepository
    {
        public int GetNilaiBiaya(FinoDBContext context, Datalib.Entity.RefBiaya pBiaya, int pOption)
        {
            var nilaiBiaya = context.BiayaNilaiOptions.Where(e => e.biaya_id.Equals(pBiaya.biaya_id)).ToList();
            if (nilaiBiaya.Count.Equals(1))
            {
                return nilaiBiaya.First().nilai;
            }
            else
            {
                var nBiaya = nilaiBiaya.Where(e => e.option.Equals(pOption)).FirstOrDefault();
                if (nBiaya != null)
                {
                    return nBiaya.nilai;
                }
            }
            return 0;
        }
    }
}
