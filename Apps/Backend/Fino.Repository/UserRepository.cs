﻿using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class UserRepository : IUserRepository
    {
        FinoDBContext _context;

        public UserRepository()
        {
            _context = new FinoDBContext();
        }

        public SysUser UserLogon(string p_UserName, string p_Password)
        {
            SysUser user = (from a in _context.Users
                            where a.name.ToLower().Equals(p_UserName.ToLower()) &&
                            a.password.ToLower().Equals(p_Password.ToLower())
                            select a).FirstOrDefault();

            if (user != null)
            {
                return user;
            }
            else
            {
                return null;
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
