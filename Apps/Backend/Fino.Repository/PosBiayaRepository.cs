﻿using Fino.Datalib.Dbmodel;
using Fino.Model;
using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class PosBiayaRepository : IPosBiayaRepository
    {
        FinoDBContext _context;

        

        public PosBiayaRepository()
        {
            _context = new FinoDBContext();
        }

        public List<RekapitulasiPiutangDanPenerimaanModel> GetRekapitulasiPiutangDanPenerimaan(DateTime p_FromDate, DateTime p_ToDate)
        {
            var sqlCmd = new StringBuilder("select b.nama as NamaBiaya ")
                            .Append(", sum(case ispaid ")
                            .Append("when 0 then 0 ")
                            .Append("when 1 then p.nilai ")
                            .Append("end) as Penerimaan ")
                            .Append(", sum(case ispaid ")
                            .Append("when 0 then p.nilai - p.nilaipotongan ")
                            .Append("when 1 then 0 ")
                            .Append("end) as Piutang ")
                            .Append("from PosBiayas p inner join ")
                            .Append("RefBiayas b on p.Biaya_Id = b.biaya_id ")
                            .Append("where p.jtempo >= @p0 ")
                            .Append("and ")
                            .Append("p.jtempo <= @p1 ")
                            .Append("group by b.nama ");

            List<RekapitulasiPiutangDanPenerimaanModel> resultSql = _context.Database
                .SqlQuery<RekapitulasiPiutangDanPenerimaanModel>(
                    sqlCmd.ToString(),
                    p_FromDate.ToString("yyyy/MM/dd"),
                    p_ToDate.ToString("yyyy/MM/dd")).ToList();

            return resultSql;
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void CalculatePotongan(FinoDBContext context, double pNilaiBiaya, 
            DateTime pJTempo, RefBiaya pBiaya, RefSiswa pSiswa,
            out double pPotongan, out bool pIsPersen, out double pNilaiPotongan)
        {
            
            double potongan = 0;
            bool isPersen = false;
            double NilaiPotongan = 0;
            

            if (pSiswa.Potongans != null && pSiswa.Potongans.Count > 0)
            {
                var potonganBiaya = pSiswa.Potongans.Where(e => e.biaya_id.Equals(pBiaya.biaya_id)).FirstOrDefault();
                if (potonganBiaya != null &&
                    (pJTempo.Date >= potonganBiaya.mulai_tanggal.Date
                     && pJTempo.Date.Date <= potonganBiaya.hingga_tanggal.Date))
                {
                    potongan = potonganBiaya.nilai;
                    isPersen = potonganBiaya.persen;
                    if (isPersen)
                    {
                        NilaiPotongan = pNilaiBiaya * (potongan / 100);
                    }
                    else
                    {
                        NilaiPotongan = potongan;
                    }
                }
            }
            pPotongan = potongan;
            pIsPersen = isPersen;
            pNilaiPotongan = NilaiPotongan;
        }

        private void GenerateDefault(FinoDBContext context, DateTime currentDate, int pOption, RefBiaya pBiaya, RefSiswa pSiswa)
        {
            IBiayaNilaiOptionRepository biayaNilaiOption = new BiayaNilaiOptionRepository();
            double nilaiBiaya = (double)biayaNilaiOption.GetNilaiBiaya(context, pBiaya, pOption);
            var jTempo = new DateTime(currentDate.Year, currentDate.Month, pBiaya.jt_tanggal);
            var deskripsi = new StringBuilder(pBiaya.nama).ToString();

            double potongan = 0;
            bool isPersen = false;
            double nilaiPotongan = 0;
            CalculatePotongan(context, nilaiBiaya, jTempo, pBiaya, pSiswa, out potongan, out isPersen, out nilaiPotongan);

            var posBiaya = new PosBiaya
            {
                Biaya_Id = pBiaya.biaya_id,
                Deskripsi = deskripsi,
                IsActive = true,
                IsPaid = false,
                JTempo = jTempo,
                Nilai = nilaiBiaya,
                Potongan = potongan,
                IsPersen = isPersen,
                NilaiPotongan = nilaiPotongan,
                Status_Id = 0
            };
            pSiswa.PosBiayas.Add(posBiaya);
        }

        private void GenerateBulanan(FinoDBContext context, DateTime pStart, DateTime pEnd, int pOption, RefBiaya pBiaya, RefSiswa pSiswa)
        {
            for (int i = 0; pStart.AddMonths(i) <= pEnd; i++)
            {
                var currentDate = pStart.AddMonths(i);
                IBiayaNilaiOptionRepository biayaNilaiOption = new BiayaNilaiOptionRepository();
                double nilaiBiaya = biayaNilaiOption.GetNilaiBiaya(context, pBiaya, pOption);
                var jTempo = new DateTime(currentDate.Year, currentDate.Month, pBiaya.jt_tanggal);
                var deskripsi = new StringBuilder(pBiaya.nama).Append(" ").Append(jTempo.ToString("MMMM-yyyy")).ToString();
                
                double potongan = 0;
                bool isPersen = false;
                double nilaiPotongan = 0;
                CalculatePotongan(context, nilaiBiaya, jTempo, pBiaya, pSiswa, out potongan, out isPersen, out nilaiPotongan);

                var posBiaya = new PosBiaya
                {
                    Biaya_Id = pBiaya.biaya_id,
                    Deskripsi = deskripsi,
                    IsActive = true,
                    IsPaid = false,
                    JTempo = jTempo,
                    Nilai = nilaiBiaya,
                    Potongan = potongan,
                    IsPersen = isPersen,
                    NilaiPotongan = nilaiPotongan,
                    Status_Id = 0
                };
                pSiswa.PosBiayas.Add(posBiaya);
            }
        }


        public void GeneratePosBiaya(FinoDBContext context, DateTime pStart, DateTime pEnd, int pOption, RefBiaya pBiaya, RefSiswa pSiswa)
        {
            switch (pBiaya.repetisi)
            {
                case (int)RepetisiEnum.BULANAN:
                    GenerateBulanan(context, pStart, pEnd, pOption, pBiaya, pSiswa);
                    break;

                default:
                    GenerateDefault(context, DateTime.Now.Date, pOption, pBiaya, pSiswa);
                    break;
            }
        }
    }
}
