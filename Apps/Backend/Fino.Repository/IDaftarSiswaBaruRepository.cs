﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Model;

namespace Fino.Repository
{
    public interface IDaftarSiswaBaruRepository
    {
        DaftarSiswaBaruModel TambahSiswaBaru(DaftarSiswaBaruModel model);
    }
}
