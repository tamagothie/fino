﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using Fino.Lib.Core;
using Fino.Model;

namespace Fino.Repository
{
    public class DaftarSiswaBaruRepository : IDaftarSiswaBaruRepository
    {
        public DaftarSiswaBaruModel TambahSiswaBaru(DaftarSiswaBaruModel model)
        {
            var returnModel = model;
            var siswaKelasList = new List<SiswaKelas>();
            IPosBiayaRepository posBiayaRepo = new PosBiayaRepository();
            IGrupBiayaRepository grupBiayaRepo = new GrupBiayaRepository();
            var siswaKelasEntity = new SiswaKelas
            {
                kelas_id = model.Kelas_Id,
                is_siswa_baru = true,
                mulai_tanggal = model.Mulai_Tanggal.Date,
                tahun_ajaran_id = model.TahunAjaran_Id,
                tgl_daftar = DateTime.Now
            };
            siswaKelasList.Add(siswaKelasEntity);

            var siswaEntity = new RefSiswa
            {
                nama = model.Nama,
                siswa_code = model.Code,
                jkelamin = model._JKelamin,
                SiswaKelas = siswaKelasList,
                PosBiayas = new List<PosBiaya>()
            };

            using (var context = new FinoDBContext())
            {
                siswaEntity = context.RefSiswas.Add(siswaEntity);
                
                // Get Tahun Ajaran range
                var ta = context.RefTahunAjarans.Find(model.TahunAjaran_Id);
                var kelas = context.RefKelases.Find(model.Kelas_Id);

                // Generate pos biaya here
                var endOfTA = new DateTime(ta.hingga_tahun, ta.hingga_bulan, model.Mulai_Tanggal.Day);
                var biayaList = grupBiayaRepo.GetBiayaOfGrupCode(context, AppResource.SYS_GRUPBIAYA_SISWA_BARU);

                foreach (var b in biayaList)
                {
                    posBiayaRepo.GeneratePosBiaya(context, model.Mulai_Tanggal.Date, endOfTA, kelas.tingkat, b, siswaEntity);
                }

                context.SaveChanges();
                returnModel.Siswa_Id = siswaEntity.siswa_id;
            }

            return returnModel;
        }

        
    }
}
