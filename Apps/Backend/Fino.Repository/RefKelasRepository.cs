﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;

namespace Fino.Repository
{
    public class RefKelasRepository : IRefKelasRepository
    {
        public List<RefKelas> SelectAllKelas()
        {
            List<RefKelas> result = null;
            using(FinoDBContext context = new FinoDBContext())
            {
                result = context.RefKelases.ToList();
            }

            return result;
        }
    }
}
