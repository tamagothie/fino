﻿using Fino.Datalib.Entity;
using Fino.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public interface IUserRepository : IDisposable
    {
        SysUser UserLogon(string p_UserName, string p_Password);
    }
}
