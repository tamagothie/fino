﻿using Fino.Datalib.Dbmodel;
using Fino.Datalib.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Repository
{
    public class KonfigurasiBiayaRepository : IKonfigurasiBiayaRepository
    {
        FinoDBContext _context;

        public KonfigurasiBiayaRepository()
        {
            _context = new FinoDBContext();
        }

        public List<RefBiaya> KonfigurasiBiayaData()
        {
            return (from a in _context.RefBiayas
                    select a).ToList();
        }

        public void AddKonfigurasiBiayaData(RefBiaya p_RefBiaya)
        {
            _context.RefBiayas.Add(p_RefBiaya);
        }

        public void UpdateKonfigurasiBiayaData(RefBiaya p_RefBiaya)
        {
            RefBiaya updateObj = _context.RefBiayas.Where(x => x.biaya_id == p_RefBiaya.biaya_id).FirstOrDefault();

            updateObj.nama = p_RefBiaya.nama;
            updateObj.code = p_RefBiaya.code;
            updateObj.repetisi = p_RefBiaya.repetisi;
            updateObj.jt_tanggal = p_RefBiaya.jt_tanggal;
            updateObj.jt_bulan = p_RefBiaya.jt_bulan;
            updateObj.aktif = p_RefBiaya.aktif;
            updateObj.mulai_tanggal = p_RefBiaya.mulai_tanggal;
        }

        public void DeleteKonfigurasiBiayaData(RefBiaya p_RefBiaya)
        {
            RefBiaya deletedObj = _context.RefBiayas.Where(x => x.biaya_id == p_RefBiaya.biaya_id).FirstOrDefault();

            if (deletedObj != null)
            {
                _context.RefBiayas.Remove(deletedObj);
            }
        }

        public List<RefGrupBiaya> GrupBiayaData()
        {
            return (from a in _context.RefGrupBiayas
                    select a).ToList();
        }

        public void AddGrupBiayaData(RefGrupBiaya p_RefGrupBiaya)
        {
            _context.RefGrupBiayas.Add(p_RefGrupBiaya);
        }

        public void DeleteGrupBiayaData(RefGrupBiaya p_RefGrupBiaya)
        {
            RefGrupBiaya deletedObj = _context.RefGrupBiayas.Where(x => x.Grupbiaya_id == p_RefGrupBiaya.Grupbiaya_id).FirstOrDefault();

            if (deletedObj != null)
            {
                _context.RefGrupBiayas.Remove(deletedObj);
            }
        }

        public void UpdateGrupBiayaData(RefGrupBiaya p_RefGrupBiaya)
        {
            RefGrupBiaya updateObj = _context.RefGrupBiayas.Where(x => x.Grupbiaya_id == p_RefGrupBiaya.Grupbiaya_id).FirstOrDefault();

            updateObj.nama = p_RefGrupBiaya.nama;
            updateObj.code = p_RefGrupBiaya.code;
        }

        public void UpdateMappingGrupBiayaData(List<GrupBiaya> grupBiayas, RefGrupBiaya p_refGrupBiaya)
        {
            if (grupBiayas.Count > 0)
            {
                foreach (GrupBiaya item in grupBiayas)
                {
                    GrupBiaya existing = (from a in _context.GrupBiayas
                                          where a.GrupBiaya_Id == item.GrupBiaya_Id && a.Biaya_Id == item.Biaya_Id
                                          select a).FirstOrDefault();

                    if (existing != null)
                    {
                        _context.GrupBiayas.Remove(existing);
                        _context.GrupBiayas.Add(item);
                    }
                    else
                    {
                        _context.GrupBiayas.Add(item);
                    }
                }
            }
            else
            {
                List<GrupBiaya> existing = (from a in _context.GrupBiayas
                                      where a.GrupBiaya_Id == p_refGrupBiaya.Grupbiaya_id
                                      select a).ToList();

                _context.GrupBiayas.RemoveRange(existing);
            }
        }

        public List<GrupBiaya> GetMappingGrupBiayaData(int p_GrupBiayaId)
        {
            return (from a in _context.GrupBiayas
                    where a.GrupBiaya_Id == p_GrupBiayaId
                    select a).ToList();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}