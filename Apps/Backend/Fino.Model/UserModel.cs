﻿using Fino.Lib.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class UserModel : BaseModel
    {
        public static readonly string UsernamePropertyName = "Username";
        public static readonly string PasswordPropertyName = "Password";

        string _username;
        string _password;

        [Required]
        [StringLength(15)]
        public string Username 
        { 
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(UsernamePropertyName));
                ValidateProperty(value, UsernamePropertyName);
            }
        }

        [Required]
        [StringLength(100)]
        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                InvokePropertyChanged(new PropertyChangedEventArgs(PasswordPropertyName));
                ValidateProperty(value, PasswordPropertyName);
            }
        }
    }
}
