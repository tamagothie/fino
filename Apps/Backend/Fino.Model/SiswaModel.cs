﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Model
{
    public class SiswaModel
    {
        public string Nama { get; set; }

        public string Alamat { get; set; }

        public DateTime TanggalLahir { get; set; }
    }
}
