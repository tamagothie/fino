﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class BiayaNilaiOptionConfiguration
    {
        internal static void ConfigureBiayaNilaiOption(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BiayaNilaiOption>().HasKey(e => e.biayanilaioption_id);
            modelBuilder.Entity<BiayaNilaiOption>().Property(e => e.biayanilaioption_id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<BiayaNilaiOption>()
                .HasRequired(e => e.Biaya)
                .WithMany(e => e.BiayaNilaiOptions)
                .HasForeignKey(e => e.biaya_id);
        }
    }
}
