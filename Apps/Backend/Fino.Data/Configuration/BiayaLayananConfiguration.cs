﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class BiayaLayananConfiguration
    {
        internal static void ConfigureBiayaLayanan(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BiayaLayanan>().HasKey(e => e.biaya_id);
            modelBuilder.Entity<BiayaLayanan>()
                .HasRequired(e => e.Biayas)
                .WithOptional(e => e.BiayaOfLayanan);
        }
    }
}
