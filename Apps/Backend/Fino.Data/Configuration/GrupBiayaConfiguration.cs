﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;

namespace Fino.Datalib.Configuration
{
    internal static class GrupBiayaConfiguration
    {
        internal static void ConfigureRefGrupBiaya(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GrupBiaya>().HasKey(e => e.Grup_Id);
            modelBuilder.Entity<GrupBiaya>().Property(e => e.Grup_Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<GrupBiaya>()
                .HasRequired(e => e.Biaya)
                .WithMany(e => e.GrupBiayas)
                .HasForeignKey(e => e.Biaya_Id);

            modelBuilder.Entity<GrupBiaya>()
                .HasRequired(e => e.Grup)
                .WithMany(e => e.GrupBiayas)
                .HasForeignKey(e => e.GrupBiaya_Id);
        }
    }
}
