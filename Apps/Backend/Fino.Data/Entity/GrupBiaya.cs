﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fino.Datalib.Entity
{
    public class GrupBiaya
    {
        public int Grup_Id { get; set; }
        public int GrupBiaya_Id { get; set; }
        public int Biaya_Id { get; set; }

        public virtual RefBiaya Biaya { get; set; }
        public virtual RefGrupBiaya Grup { get; set; }
    }
}
