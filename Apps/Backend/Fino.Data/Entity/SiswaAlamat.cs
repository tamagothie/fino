﻿
namespace Fino.Datalib.Entity
{
    public class SiswaAlamat
    {
        public int siswa_id {get;set;}
        public string alamat {get;set;}
        public string kecamatan {get;set;}
        public string kota {get;set;}
        public string propinsi {get;set;}

        public virtual RefSiswa Siswas { get; set; }
    }
}
