﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Fino.Datalib.Entity;
using Fino.Lib.Core;

namespace Fino.Datalib.Dbmodel
{
    public class FinoDBInitializer : CreateDatabaseIfNotExists<FinoDBContext>
    {
        protected override void Seed(FinoDBContext context)
        {

            SeedUser(context);

            var bsb = SeedGrupBiaya(context, "Biaya Siswa Baru", "BSB");
            var bdu = SeedGrupBiaya(context, "Biaya Daftar Ulang", "BDU");

            // TODO: change repetisi enum to indonesian
            var spp = SeedBiaya(context, "SPP", "SPP", 0, 10, (int)RepetisiEnum.BULANAN);
            var ltk = SeedBiaya(context, "Listrik", "LTK", 0, 10, (int)RepetisiEnum.BULANAN);
            var bku = SeedBiaya(context, "Buku", "BKU", 0, 10, (int)RepetisiEnum.NONE);
            var bgn = SeedBiaya(context, "Pembangunan", "BGN", 0, 10, (int)RepetisiEnum.NONE);
            var kgt = SeedBiaya(context, "Kegiatan", "KGT", 0, 10, (int)RepetisiEnum.NONE);
            var sgm = SeedBiaya(context, "Seragam", "SGM", 0, 10, (int)RepetisiEnum.NONE);
            var kat = SeedBiaya(context, "Katering", "KAT", 0, 10, (int)RepetisiEnum.NONE);
            var jpt = SeedBiaya(context, "Antar Jemput", "JPT", 0, 10, (int)RepetisiEnum.NONE);
            var pomg = SeedBiaya(context, "POMG", "POMG", 0, 10, (int)RepetisiEnum.NONE);
            var zml = SeedBiaya(context, "Zakat Mal", "ZML", 0, 10, (int)RepetisiEnum.NONE);
            var zft = SeedBiaya(context, "Zakat Fitrah", "ZFT", 0, 10, (int)RepetisiEnum.NONE);

            var ljpt = SeedRefLayanan(context, "Antar Jemput", "JPT");
            var lkat = SeedRefLayanan(context, "Katering", "KAT");
            SeedRefKelas(context);
            context.SaveChanges();

            SeedBiayaToGrup(context, spp, bsb);
            SeedBiayaToGrup(context, ltk, bsb);
            SeedBiayaToGrup(context, bku, bsb);
            SeedBiayaToGrup(context, bgn, bsb);
            SeedBiayaToGrup(context, kgt, bsb);
            SeedBiayaToGrup(context, sgm, bsb);
            SeedBiayaToGrup(context, pomg, bsb);

            SeedBiayaToGrup(context, spp, bdu);
            SeedBiayaToGrup(context, ltk, bdu);
            SeedBiayaToGrup(context, bku, bdu);
            SeedBiayaToGrup(context, bgn, bdu);
            SeedBiayaToGrup(context, kgt, bdu);
            SeedBiayaToGrup(context, sgm, bdu);
            SeedBiayaToGrup(context, pomg, bdu);

            SeedRefTahunAjaran(context);


            SeedBiayaLayanan(context, jpt, ljpt);
            SeedBiayaLayanan(context, kat, lkat);
            for (int i = 1; i <= 12; i++)
            {
                SeedBiayaNilaiOption(context, spp, i, 100000);
            }

            SeedBiayaNilaiOption(context, sgm, 0, 100000);
            SeedBiayaNilaiOption(context, bku, 0, 100000);
            SeedBiayaNilaiOption(context, bgn, 0, 5000000);
            SeedBiayaNilaiOption(context, pomg, 0, 100000);
            SeedBiayaNilaiOption(context, ltk, 0, 100000);
            SeedBiayaNilaiOption(context, kgt, 0, 1000000);

            context.SaveChanges();
        }

        private void SeedBiayaNilaiOption(FinoDBContext context, RefBiaya pBiaya, int pOption, int pNilai)
        {
            var bNilai = new BiayaNilaiOption
            {
                biaya_id = pBiaya.biaya_id,
                nilai = pNilai,
                option = pOption
            };

            context.BiayaNilaiOptions.Add(bNilai);
        }

        private void SeedRefKelas(FinoDBContext context)
        {
            var sub = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
            for (var i = 0; i < 12; i++)
            {
                foreach (var s in sub)
                {
                    var sb = new StringBuilder(string.Empty);
                    var refKelas = new RefKelas
                    {
                        nama = sb.Append((i + 1)).Append(s).ToString(),
                        kelas_code = sb.Append(i).Append(s).ToString(),
                        tingkat = i + 1
                    };
                    context.RefKelases.Add(refKelas);
                }
            }
        }

        private void SeedBiayaLayanan(FinoDBContext context, RefBiaya pBiaya, RefLayanan pLayanan)
        {
            var biayaLayanan = new BiayaLayanan()
            {
                layanan_id = pLayanan.layanan_id,
                biaya_id = pBiaya.biaya_id
            };
            context.BiayaLayanans.Add(biayaLayanan);
        }

        private RefLayanan SeedRefLayanan(FinoDBContext context, string pNama, string pCode)
        {
            var refLayanan = new RefLayanan
            {
                nama = pNama,
                code = pCode
            };
            refLayanan = context.RefLayanans.Add(refLayanan);
            return refLayanan;
        }

        private void SeedRefTahunAjaran(FinoDBContext context)
        {
            int current = DateTime.Now.AddYears(-1).Year;

            for (int i = current; i <= current + 10; i++)
            {
                for (int sem = 0; sem < 2; sem++)
                {
                    var sb = new StringBuilder("Tahun Ajaran ")
                            .Append((i)).Append("/").Append(i + 1)
                            .Append(" Sem. ").Append((sem + 1));
                    var refTh = new RefTahunAjaran
                    {
                        nama = sb.ToString(),
                        mulai_bulan = (sem.Equals(0) ? 7 : 1),
                        mulai_tahun = (sem.Equals(0) ? (i) : i + 1),
                        hingga_bulan = (sem.Equals(0) ? 12 : 6),
                        hingga_tahun = (sem.Equals(0) ? (i) : i+1),
                        semcawu = sem + 1,
                        status = 0
                    };

                    context.RefTahunAjarans.Add(refTh);
                }
            }
        }

        private void SeedBiayaToGrup(FinoDBContext context, RefBiaya pBiaya, RefGrupBiaya pGrup)
        {
            var grupBiaya = new GrupBiaya
            {
                GrupBiaya_Id = pGrup.Grupbiaya_id,
                Biaya_Id = pBiaya.biaya_id
            };

            context.GrupBiayas.Add(grupBiaya);
        }

        private RefGrupBiaya SeedGrupBiaya(FinoDBContext context, string pNama, string pCode)
        {
            var refGrupBiaya = new RefGrupBiaya
            {
                nama = pNama,
                code = pCode,
                active = true
            };
            refGrupBiaya = context.RefGrupBiayas.Add(refGrupBiaya);
            return refGrupBiaya;
        }

        private RefBiaya SeedBiaya(FinoDBContext context, string pNama, string pCode, int pJtBulan, int pJtTanggal, int pRepetisi)
        {
            var refBiaya = new RefBiaya
            {
                nama = pNama,
                code = pCode,
                jt_bulan = pJtBulan,
                jt_tanggal = pJtTanggal,
                mulai_tanggal = DateTime.Now.Date,
                repetisi = pRepetisi,
                aktif = true
            };
            refBiaya = context.RefBiayas.Add(refBiaya);
            return refBiaya;
        }

        private void SeedUser(FinoDBContext context)
        {
            var ContactList = new List<SysUserContact>();
            ContactList.Add(new SysUserContact
            {
                contact = "fino@nitrocodeus.com",
                type = (int)ContactTypeEnum.EMAIL
            });

            context.Users.Add(new SysUser
            {
                name = "system",
                password = "incrediblehulk",
                Contacts = ContactList
            });
        }
    }
}
