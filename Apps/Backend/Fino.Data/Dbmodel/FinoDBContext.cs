﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Fino.Datalib.Configuration;
using Fino.Datalib.Entity;
using Fino.Lib.Core;

namespace Fino.Datalib.Dbmodel
{
    public class FinoDBContext : DbContext
    {
        public DbSet<SysUser> Users { get; set; }
        public DbSet<SysUserContact> UserContacts { get; set; }
        public DbSet<RefBiaya> RefBiayas { get; set; }
        public DbSet<RefKelas> RefKelases { get; set; }
        public DbSet<RefLayanan> RefLayanans { get; set; }
        public DbSet<RefSiswa> RefSiswas { get; set; }
        public DbSet<RefTahunAjaran> RefTahunAjarans { get; set; }
        public DbSet<SiswaAlamat> SiswaAlamats { get; set; }
        public DbSet<SiswaKelas> SiswaKelases { get; set; }
        public DbSet<AgtLayanan> AgtLayanans { get; set; }
        public DbSet<BiayaLayanan> BiayaLayanans { get; set; }
        public DbSet<Potongan> Potongans { get; set; }
        public DbSet<RefGrupBiaya> RefGrupBiayas { get; set; }
        public DbSet<GrupBiaya> GrupBiayas { get; set; }
        public DbSet<PosBiaya> PosBiayas { get; set; }
        public DbSet<BiayaNilaiOption> BiayaNilaiOptions { get; set; }

        static FinoDBContext()
        {
            Database.SetInitializer<FinoDBContext>(new FinoDBInitializer());
            using (FinoDBContext db = new FinoDBContext())
            {
                db.Database.Initialize(false);
            }
        }

        public FinoDBContext()
            : base(AppResource.CONN_STRING_NAME)
        {}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            ConfigureModel(modelBuilder);
        }

        private void ConfigureModel(DbModelBuilder modelBuilder)
        {
            SysUserConfiguration.ConfigureSysUser(modelBuilder);
            SysUserContactConfiguration.ConfigureUserContact(modelBuilder);
            RefBiayaConfiguration.ConfigureBiaya(modelBuilder);
            RefKelasConfiguration.ConfigureRefKelas(modelBuilder);
            RefLayananConfiguration.ConfigureRefLayanan(modelBuilder);
            RefSiswaConfiguration.ConfigureRefSiswa(modelBuilder);
            RefTahunAjaranConfiguration.ConfigureRefTahunAjaran(modelBuilder);
            RefGrupBiayaConfiguration.ConfigureRefGrupBiaya(modelBuilder);

            SiswaAlamatConfiguration.ConfigureSiswaAlamat(modelBuilder);
            SiswaKelasConfiguration.ConfigureSiswaKelas(modelBuilder);
            AgtLayananConfiguration.ConfigureAgtLayanan(modelBuilder);
            BiayaLayananConfiguration.ConfigureBiayaLayanan(modelBuilder);
            PotonganConfiguration.ConfigurePotongan(modelBuilder);
            GrupBiayaConfiguration.ConfigureRefGrupBiaya(modelBuilder);
            PosBiayaConfiguration.ConfigurePosBiaya(modelBuilder);
            BiayaNilaiOptionConfiguration.ConfigureBiayaNilaiOption(modelBuilder);
        }
    }
}
